//
//  RegistrationStepPhoneVC.swift
//  Kredo
//
//  Created by Josef Antoni on 10.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class RegistrationStepPhoneVC: UIViewController, AlertMessage {
    
    fileprivate var _seenPromoScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScene()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !_seenPromoScreen {
            presentModal()
        }
    }
    
    fileprivate func presentModal(){
        _seenPromoScreen = true
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "PromoCodeVC") as! PromoCodeVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
    
    fileprivate func setupScene(){
        self.navigationController?.isNavigationBarHidden = true
        self.phoneCodeView.layer.cornerRadius = 20
        self.enterPhoneBtn.layer.cornerRadius = 20
        
        self.phoneCodeView.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.enterPhoneBtn.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
    }
    
    @IBOutlet weak var phoneCodeTxtField: UITextField!
    @IBOutlet weak var phoneCodeView: UIView!
    @IBOutlet weak var enterPhoneBtn: UIButton!
}

extension RegistrationStepPhoneVC {
    
    @IBAction func enterPhoneBtn(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            if !phoneCodeTxtField.text!.isEmpty {
                LoginRequests().conectPhoneNumberToAcc(phoneNumber: phoneCodeTxtField.text!) { (done) in
                    if done {
                        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "TarifSelectionVC") as! TarifSelectionVC
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.navigationController?.pushViewController(openNewVC, animated: true)
                        });
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                    }
                }
            } else {
                phoneCodeView.shake(count: 4, for: 0.3, withTranslation: 8)
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @IBAction func noPhoneBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "TarifSelectionVC") as! TarifSelectionVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func skipBtn(_ sender: Any) {
    }
}
