//
//  NumberOfDialVC.swift
//  Kredo
//
//  Created by Josef Antoni on 10.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class NumberOfDialVC: UIViewController {
    
    fileprivate var _totalOfNumbers: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    fileprivate func _calculate(){
        if _totalOfNumbers == 1 {
            calculationLabel.text = _totalOfNumbers.description
        } else {
            calculationLabel.text = "1 + \(_totalOfNumbers-1)"
        }
    }
    
    fileprivate func _setupScene(){
        self.proceedBtn.layer.cornerRadius = 20
        self.proceedBtn.layer.borderWidth = 1
        self.proceedBtn.layer.borderColor = UIColor.white.cgColor
    }
    
    fileprivate func _reserveDialNumbers(){
        SwiftLoader.show(animated: true)
        ConsuptionRequests().howManyPhoneNumbersUserNeed(phoneNumbers: self._totalOfNumbers) { (done) in
            if done {
                ConsuptionRequests().getSumarizationInfo { (done, minutes, sms, data, monthlySpend, cheaper, infoText, bonus, bonusForSharing) in
                    SwiftLoader.hide()
                        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "SumarizationVC") as! SumarizationVC
                        openNewVC.minutes = minutes
                        openNewVC.sms = sms
                        openNewVC.data = data
                        openNewVC.monthlySpend = monthlySpend
                        openNewVC.cheaper = cheaper
                        openNewVC.bonus = bonus
                        openNewVC.bonusForSharing = bonusForSharing

                        DispatchQueue.main.async(execute: { () -> Void in
                            self.navigationController?.pushViewController(openNewVC, animated: true)
                        });
                }
            } else {
                SwiftLoader.hide()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
            }
        }
    }
    
    @IBOutlet weak var proceedBtn: UIButton!
    @IBOutlet weak var calculationLabel: UILabel!
}

extension NumberOfDialVC {
    
    @IBAction func plusBtn(_ sender: Any) {
        _totalOfNumbers += 1
        _calculate()
    }
    
    @IBAction func minusBtn(_ sender: Any) {
        if _totalOfNumbers > 1 {
            _totalOfNumbers -= 1
            _calculate()
        }
    }
    
    @IBAction func proceedBtn(_ sender: Any) {
        self._reserveDialNumbers()
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}
