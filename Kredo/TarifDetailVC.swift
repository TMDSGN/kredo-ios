//
//  TarifDetailVC.swift
//  Kredo
//
//  Created by Josef Antoni on 11.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class TarifDetailVC: UIViewController {
    
    var detail: TarifDetailData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
        if detail != nil {
            _setupData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if detail != nil {
            switch detail!.cross {
            case "0":
                self.tarifBgBotConstraint.isActive = true
                self.wifiBgBotConstraint.isActive = false
                self.tvBgBotConstraint.isActive = false
                self.wifiBg.isHidden = true
                self.tvBg.isHidden = true
            case "1":
                self.tvBg.isHidden = true
                self.wifiBgBotConstraint.isActive = true
                self.tvBgBotConstraint.isActive = false
            case "2":
                self.wifiBg.isHidden = true
                self.tvBgBotConstraint.isActive = true
                self.wifiBgBotConstraint.isActive = false
                self.tvBgTotarifTopConstraint.isActive = true
                self.tvBgToWifiTopConstraint.isActive = false
            default:
                break
            }
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func _setupScene(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.backBtn.tintColor = UIColor.clear
        
        let lightGray = UIColor(red: 239/255.0, green: 239/255.0, blue: 239/255.0, alpha: 1.0)
        //rounded corners
        self.tarifBg.layer.cornerRadius = 20
        self.wifiBg.layer.cornerRadius = 20
        self.tvBg.layer.cornerRadius = 20
        //add shadow
        
        self.callingContainer.layer.cornerRadius = 20
        self.callingContainer.layer.borderWidth = 1
        self.callingContainer.layer.borderColor = lightGray.cgColor
        
        self.textingContainer.layer.cornerRadius = 20
        self.textingContainer.layer.borderWidth = 1
        self.textingContainer.layer.borderColor = lightGray.cgColor
        
        self.dataContainer.layer.cornerRadius = 20
        self.dataContainer.layer.borderWidth = 1
        self.dataContainer.layer.borderColor = lightGray.cgColor
        
        self.wifiContainer.layer.cornerRadius = 20
        self.wifiContainer.layer.borderWidth = 1
        self.wifiContainer.layer.borderColor = lightGray.cgColor
        
        self.tvContainer.layer.cornerRadius = 20
        self.tvContainer.layer.borderWidth = 1
        self.tvContainer.layer.borderColor = lightGray.cgColor
        
    }
    
    fileprivate func _setupData(){
        let str = NSAttributedString(string: detail!.name, attributes: [
            NSStrokeColorAttributeName : UIColor(red: 134/255.0, green: 229/255.0, blue: 174/255.0, alpha: 1.0),
            NSStrokeWidthAttributeName : -5,
            NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 15)!,
            NSForegroundColorAttributeName : UIColor.white
            ])
        self.newTopTitle.attributedText = str
        self.infoTextLabel.text = detail?.info
        if detail!.freeMinAll == "neomezeně" {
            self.freeMinAllLabel.text = "\u{221E} min"
        } else {
            self.freeMinAllLabel.text = detail!.freeMinAll + " min"
        }
        if detail!.freeMinOwn == "neomezeně" {
            self.freeMinOwnLabel.text = "\u{221E} min"
        } else {
            self.freeMinOwnLabel.text = detail!.freeMinOwn + " min"
        }
        if detail!.priceCallAll == "neomezeně" {
            self.priceCallAllLabel.text = "\u{221E} min"
        } else {
            self.priceCallAllLabel.text = detail!.priceCallAll + " Kč"
        }
        if detail!.priceCallOwn == "neomezeně" {
            self.priceCallOwnLabel.text = "\u{221E} min"
        } else {
            self.priceCallOwnLabel.text = detail!.priceCallOwn + " Kč"
        }
        if detail!.freeSmsAll == "neomezeně" {
            self.freeSmsAllLabel.text = "\u{221E} min"
        } else {
            self.freeSmsAllLabel.text = detail!.freeSmsAll + " min"
        }
        if detail!.freeSmsOwn == "neomezeně" {
            self.freeSmsOwnLabel.text = "\u{221E} min"
        } else {
            self.freeSmsOwnLabel.text = detail!.freeSmsOwn + " min"
        }
        self.priceSmsAllLabel.text = detail!.priceSmsAll + " Kč"
        self.priceSmsOwnLabel.text = detail!.priceSmsOwn + " Kč"
        self.dataSpeedLabel.text = detail?.dataSpeed
        self.freeDataLabel.text = detail!.freeData + "GB"
        
        self.wifiDataLabel.text = detail!.wifiData + "GB"
        self.wifiSpeedLabel.text = detail!.wifiSpeed
        
        self.tvProgramsLabel.text = detail!.tvChannels
        self.tvDevicesLabel.text = detail!.tvDevices
        
    }
    
    @IBOutlet weak var callingContainer: UIView!
    @IBOutlet weak var textingContainer: UIView!
    @IBOutlet weak var dataContainer: UIView!
    @IBOutlet var wifiContainer: UIView!
    @IBOutlet var tvContainer: UIView!
    
    @IBOutlet var tarifBg: UIView!
    @IBOutlet var tvBg: UIView!
    @IBOutlet var wifiBg: UIView!
    
    @IBOutlet var tarifBgBotConstraint: NSLayoutConstraint!
    @IBOutlet var wifiBgBotConstraint: NSLayoutConstraint!
    @IBOutlet var tvBgBotConstraint: NSLayoutConstraint!
    @IBOutlet var tvBgTotarifTopConstraint: NSLayoutConstraint!
    @IBOutlet var tvBgToWifiTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var wifiHeightOn: NSLayoutConstraint!
    @IBOutlet var tvHeightOn: NSLayoutConstraint!
    
    @IBOutlet var infoTextLabel: UILabel!
    
    @IBOutlet var freeMinAllLabel: UILabel!
    @IBOutlet var freeMinOwnLabel: UILabel!
    @IBOutlet var priceCallAllLabel: UILabel!
    @IBOutlet var priceCallOwnLabel: UILabel!
    @IBOutlet var freeSmsAllLabel: UILabel!
    @IBOutlet var freeSmsOwnLabel: UILabel!
    @IBOutlet var priceSmsAllLabel: UILabel!
    @IBOutlet var priceSmsOwnLabel: UILabel!
    @IBOutlet var dataSpeedLabel: UILabel!
    @IBOutlet var freeDataLabel: UILabel!
    @IBOutlet var backBtn: UIBarButtonItem!
    @IBOutlet var newTopTitle: UILabel!
    
    @IBOutlet var wifiDataLabel: UILabel!
    @IBOutlet var wifiSpeedLabel: UILabel!
    
    @IBOutlet var tvProgramsLabel: UILabel!
    @IBOutlet var tvDevicesLabel: UILabel!
}

extension TarifDetailVC {
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}
