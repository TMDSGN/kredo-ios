//
//  OffersVC.swift
//  Kredo
//
//  Created by Josef Antoni on 12.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class OffersVC: UIViewController {
    
    fileprivate var _offerData: OffersData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _registerObservers()
        _customCellRegister()
        _getOffers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.tableView.reloadData()
    }
    
    fileprivate func _getOffers(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ConsuptionRequests().getListOfOffers { (done, offerData) in
                SwiftLoader.hide()
                self._offerData = offerData
                self.tableView.reloadData()
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @objc fileprivate func openTarifDetail(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let objectId = dict["objectId"] as! String
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ConsuptionRequests().getTarifDetail(id: objectId, completionHandler: { (done, detailData) in
                SwiftLoader.hide()
                if done {
                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "TarifDetailVC") as! TarifDetailVC
                    openNewVC.detail = detailData
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(openNewVC, animated: true)
                    });
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @objc fileprivate func orderTarifDetail(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let objectId = dict["objectId"] as! String
        if Reachability.isConnectedToNetwork() {
            ConsuptionRequests().postTarifOrder(id: objectId)
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoVC") as! PersonalInfoVC
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @objc fileprivate func cantBeOrdered(_ notification: NSNotification){
        //let dict = notification.object as! NSDictionary
        //let objectId = dict["objectId"] as! String
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "NoAvailableTarifPickupVC") as! NoAvailableTarifPickupVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
    
    fileprivate func _customCellRegister(){
        let nib = UINib(nibName: "OfferCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "OfferCell")
        tableView.separatorStyle = .none
    }
    
    fileprivate func _getProfileInfo(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileRequests().getProfileInfo { (done, myProfileData) in
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                if done {
                    openNewVC.myProfileData = myProfileData
                } else {
                    openNewVC.profileComplete = false
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
                SwiftLoader.hide()
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func _registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.cantBeOrdered), name: NSNotification.Name(rawValue: "cantBeOrdered"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openTarifDetail), name: NSNotification.Name(rawValue: "openTarifDetail"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderTarifDetail), name: NSNotification.Name(rawValue: "orderTarifDetail"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cantBeOrdered"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openTarifDetail"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "orderTarifDetail"), object: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
}

extension OffersVC {
    
    @IBAction func openProfileBtn(_ sender: Any) {
        _getProfileInfo()
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

extension OffersVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self._offerData != nil {
            return self._offerData!.idArr.count + 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 100
        } else {
            return 400
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        if indexPath.row == 0 {
            let cell = UITableViewCell()
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "OfferCell") as! OfferCellVC
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            let od = self._offerData!
            if od.myMinutes == "neomezeně" {
                cell.myMinutesLabel.text = "\u{221E} min"
            } else {
                cell.myMinutesLabel.text = od.myMinutes + " min"
            }
            if od.mySms == "neomezeně" {
                cell.myTextLabel.text = "\u{221E}"
            } else {
                cell.myTextLabel.text = od.mySms
            }
            cell.myDataLabel.text = od.myData + " GB"
            cell.myCoverLabel.text = od.myCover + "%"
            
            cell.id = od.idArr[indexPath.row - 1]
            if od.minutesArr[indexPath.row - 1] == "neomezeně" {
                cell.minuteLabel.text = "\u{221E} min"
            } else {
                cell.minuteLabel.text = od.minutesArr[indexPath.row - 1] + " min"
            }
            if od.smsArr[indexPath.row - 1] == "neomezeně" {
                cell.smsLabel.text = "\u{221E}"
            } else {
                cell.smsLabel.text = od.smsArr[indexPath.row - 1]
            }
            cell.dataLabel.text = od.dataArr[indexPath.row - 1] + " GB"
            cell.coverLabel.text = od.coverArr[indexPath.row - 1] + "%"
            
            cell.totalPriceLabel.text = od.monthlySpendArr[indexPath.row - 1] + " Kč"
            cell.cheaperRedLabel.text = "Možná úspora \(od.cheaperArr[indexPath.row - 1]) Kč"
            if let cheaper = Int(od.cheaperArr[indexPath.row - 1]) {
                if cheaper < 0 {
                    cell.cheaperRedLabel.text = "Dražší o \(od.cheaperArr[indexPath.row - 1].replacingOccurrences(of: "-", with: "")) Kč"
                }
            }
            //zde se vezme ID poskytovatele a srovná se s ID poskytovatelů, kteří jsou uloženi v Session, pokud souhlasí ID tak přeruší se FOR a dosadí je podle indexu správný oprázek
            let carrierResult = od.carrierArr[indexPath.row - 1]            
            if let carrierData = Session.sharedInstance.carrierData {
                for i in 0..<carrierData.idArr.count {
                    if carrierResult == carrierData.idArr[i].description {
                        cell.providerLogo.image = UIImage(named: carrierData.nameArr[i])
                        break
                    }
                }
            }
            
            cell.blueBtn.setTitle("Jo, to chci", for: .normal)
            cell.blueBtn.backgroundColor = UIColor(red: 91/255.0, green: 151/255.0, blue: 244/255.0, alpha: 1.0)
            cell.cantBeOrdered = false
            
            if od.notAvailableTarifArr[indexPath.row - 1] {
                cell.blueBtn.setTitle("Nejde to", for: .normal)
                cell.blueBtn.backgroundColor = UIColor(red: 219/255.0, green: 42/255.0, blue: 96/255.0, alpha: 1.0)
                cell.cantBeOrdered = true
            }
            
            var wifiIcon = "wifiPackageOff"
            var tvIcon = "tvPackageOff"
            
            switch od.crossArr[indexPath.row-1] {
            case "1":
                wifiIcon = "wifiPackageOn"
            case "2":
                tvIcon = "tvPackageOn"
            case "3":
                wifiIcon = "wifiPackageOn"
                tvIcon = "tvPackageOn"
            default:
                break
            }
            cell.wifiTarificon.image = UIImage(named: wifiIcon)
            cell.pcTarifIcon.image = UIImage(named: tvIcon)

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}
