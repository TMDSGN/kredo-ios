//
//  NWSImageToken.swift
//  Kredo
//
//  Created by Josef Antoni on 04.08.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import NWSTokenView

open class NWSImageToken: NWSToken
{
    @IBOutlet weak var titleLabel: UILabel!
    
    open class func initWithTitle(_ title: String, image: UIImage? = nil) -> NWSImageToken?
    {
        if let token = UINib(nibName: "NWSImageToken", bundle:nil).instantiate(withOwner: nil, options: nil)[0] as? NWSImageToken{
            token.backgroundColor = UIColor(red: 91.0/255.0, green: 151.0/255.0, blue: 244.0/255.0, alpha: 1.0)
            let oldTextWidth = token.titleLabel.bounds.width
            token.titleLabel.text = title
            token.titleLabel.sizeToFit()
            token.titleLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
            let newTextWidth = token.titleLabel.bounds.width
            
            token.layer.cornerRadius = 5.0
            token.clipsToBounds = true
            
            // Resize to fit text
            token.frame.size = CGSize(width: token.frame.size.width+(newTextWidth-oldTextWidth), height: token.frame.height)
            token.setNeedsLayout()
            token.frame = token.frame
            
            return token
        }
        return nil
    }
}
