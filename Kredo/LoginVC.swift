//
//  LoginVC.swift
//  Kredo
//
//  Created by Josef Antoni on 04.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader
import FBSDKCoreKit
import FBSDKLoginKit
import SafariServices

class LoginVC: UIViewController, AlertMessage {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup the Network Info and create a CTCarrier object
        _setupScene()
        _regiSceneOn()
        _registerObserver()
        //get Auth token for future requests
        getAuthToken()
        loginBotConstraint.isActive = false
        regSwitchbutton.isOn = false;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        _resetEntryTxtFileld()
    }
    
    fileprivate func _resetEntryTxtFileld(){
        self.emailTxtField.text = ""
        self.passTxtField.text = ""
        self.regEmailTxt.text = ""
        self.regPassTxt.text = ""
        self.regFirstNameTxt.text = ""
        self.regSurnameTxt.text = ""
        self.regPassTxt.text = ""
        self.regPassRetypeTxt.text = ""
    }
    

    fileprivate func getAuthToken(){
        AuthRequests().getToken { (done) in
            if done {
                //get carriers data and sotre them for later use
                ConsuptionRequests().getCarriers(completionHandler: { (done, data) in
                    if done {
                        Session.sharedInstance.carrierData = data
                    }
                })
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
            }
        }
    }
    
    fileprivate func _setupScene(){
        Loader().configLoader()
        //login
        emailContainer.layer.cornerRadius = emailContainer.frame.size.height/2
        passContainer.layer.cornerRadius = passContainer.frame.size.height/2
        simpleLoginBtn.layer.cornerRadius = simpleLoginBtn.frame.size.height/2
        facebookLoginBtn.layer.cornerRadius = facebookLoginBtn.frame.size.height/2
        facebookRegBtn.layer.cornerRadius = facebookRegBtn.frame.size.height/2
        facebookRegBtn.clipsToBounds = true
        
        emailContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        passContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        simpleLoginBtn.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        facebookLoginBtn.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        
        //registration
        regFirstNameContainer.layer.cornerRadius = regFirstNameContainer.frame.size.height/2
        regSurnameContainer.layer.cornerRadius = regSurnameContainer.frame.size.height/2
        regEmailContainer.layer.cornerRadius = regEmailContainer.frame.size.height/2
        regPassContainer.layer.cornerRadius = regPassContainer.frame.size.height/2
        registrationBtn.layer.cornerRadius = registrationBtn.frame.size.height/2
        regPassRetypeContainer.layer.cornerRadius = regPassRetypeContainer.frame.size.height/2


        regFirstNameContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        regSurnameContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        regEmailContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        regPassContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        registrationBtn.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        regPassRetypeContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)


    }
    
    fileprivate func _loginSceneOn(){
        self.showLoginContainerBtn.setTitleColor(UIColor(red: 91/255.0, green: 151/255.0, blue: 244/255.0, alpha: 1.0), for: .normal)
        self.showRegistrationContinerBtn.setTitleColor(UIColor(red: 231/255.0, green: 231/255.0, blue: 231/255.0, alpha: 1.0), for: .normal)
        
        self.loginBotConstraint.isActive = true
        self.regBotConstraint.isActive = false
        UIView.animate(withDuration: 0.2, animations: {
            self.registrationContainer.alpha = 0
            self.view.layoutIfNeeded()
        }) { (done) in
            UIView.animate(withDuration: 0.5) {
                self.loginContainer.alpha = 1
                self.view.layoutIfNeeded()
            }
        }
    }
    
    fileprivate func _regiSceneOn(){
        self.showLoginContainerBtn.setTitleColor(UIColor(red: 231/255.0, green: 231/255.0, blue: 231/255.0, alpha: 1.0), for: .normal)
        self.showRegistrationContinerBtn.setTitleColor(UIColor(red: 91/255.0, green: 151/255.0, blue: 244/255.0, alpha: 1.0), for: .normal)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.loginContainer.alpha = 0
            self.view.layoutIfNeeded()
        }) { (done) in
            self.loginBotConstraint.isActive = false
            self.regBotConstraint.isActive = true
            UIView.animate(withDuration: 0.5) {
                self.registrationContainer.alpha = 1
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    fileprivate func _postRegister(){
        self.view.endEditing(true)
        if !regFirstNameTxt.text!.isEmpty {
            if !regSurnameTxt.text!.isEmpty {
                if !regEmailTxt.text!.isEmpty {
                    if regEmailTxt.text!.isValidEmail() {
                        if !regPassTxt.text!.isEmpty {
                            if !regPassTxt.text!.isEmpty {
                                if regPassRetypeTxt.text! == regPassTxt.text!{
                                    if regSwitchbutton.isOn == true{
                                        if Reachability.isConnectedToNetwork() {
                                            if Session.sharedInstance.securityToken.isEmpty {
                                                getAuthToken()
                                            } else {
                                                SwiftLoader.show(animated: true)
                                                LoginRequests().registration(firstName: regFirstNameTxt.text!, lastName: regSurnameTxt.text!, email: regEmailTxt.text!, pass: regPassTxt.text!) { (done, err) in
                                                    SwiftLoader.hide()
                                                    if done {
                                                        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "TarifSelectionVC") as! TarifSelectionVC
                                                        DispatchQueue.main.async(execute: { () -> Void in
                                                            self.navigationController?.pushViewController(openNewVC, animated: true)
                                                        });
                                                    } else {
                                                        self.showSimpleAlert(title: "Chyba", message: err)
                                                    }
                                                }
                                            }
                                        } else {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
                                        }
                                    }else{
                                        regAgreementContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                    }
                                    
                                } else {
                                    regPassContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                    regPassRetypeContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                }
                            } else {
                                regPassRetypeContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                            }
                        } else {
                            regPassContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                        }
                    } else {
                        self.showSimpleAlert(title: "Chyba", message: "Zadejte Email ve správném formátu")
                    }
                } else {
                    regEmailContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                }
            } else {
                regSurnameContainer.shake(count: 4, for: 0.3, withTranslation: 8)
            }
        } else {
            regFirstNameContainer.shake(count: 4, for: 0.3, withTranslation: 8)
        }
    }
    
    fileprivate func _postSimpleLogin(){
        self.view.endEditing(true)
        if !emailTxtField.text!.isEmpty {
            if emailTxtField.text!.isValidEmail() {
                if !passTxtField.text!.isEmpty {
                    if Reachability.isConnectedToNetwork() {
                        if Session.sharedInstance.securityToken.isEmpty {
                            getAuthToken()
                        } else {
                            SwiftLoader.show(animated: true)
                            LoginRequests().simpleLogin(email: emailTxtField.text!, pass: passTxtField.text!) { (done, errMessage) in
                                if done {
                                    LoginRequests().finishedProfile(completionHandler: { (done, finished) in
                                        if done && finished {
                                            ProfileRequests().getProfileInfo { (done, myProfileData) in
                                                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                                                if done {
                                                    openNewVC.myProfileData = myProfileData
                                                } else {
                                                    openNewVC.profileComplete = false
                                                }
                                                SwiftLoader.hide()
                                                DispatchQueue.main.async(execute: { () -> Void in
                                                    self.navigationController?.pushViewController(openNewVC, animated: true)
                                                });
                                            }
                                        } else if done && !finished {
                                            SwiftLoader.hide()
                                            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "TarifSelectionVC") as! TarifSelectionVC
                                            DispatchQueue.main.async(execute: { () -> Void in
                                                self.navigationController?.pushViewController(openNewVC, animated: true)
                                            });
                                        } else {
                                            SwiftLoader.hide()
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                                        }
                                    })
                                } else if errMessage != nil {
                                    SwiftLoader.hide()
                                    self.showSimpleAlert(title: "Chyba", message: errMessage!)
                                } else {
                                    SwiftLoader.hide()
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                                }
                            }
                        }
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
                    }
                } else {
                    passContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                }
            } else {
                self.showSimpleAlert(title: "Chyba", message: "Zadejte Email ve správném formátu")
            }
        } else {
            emailContainer.shake(count: 4, for: 0.3, withTranslation: 8)
        }
    }
    
    fileprivate func _postFacebookLogin(){
        self.view.endEditing(true)
        if Reachability.isConnectedToNetwork() {
            let login : FBSDKLoginManager = FBSDKLoginManager()
            //login.loginBehavior = FBSDKLoginBehavior.native
            login.loginBehavior = FBSDKLoginBehavior.native
            login.logIn(withReadPermissions: ["public_profile", "email"], from: self, handler: {(result, error) in
                if error != nil {
                    self.dismiss(animated: true, completion: {
                        self.showSimpleAlert(title: "Error", message: "Login via Facebook failed, please try it later.")
                    })
                } else {
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email, id"]).start(completionHandler: {(connection, result, error) -> Void in
                        if let accessToken = FBSDKAccessToken.current(){
                            SwiftLoader.show(animated: true)
                            AuthRequests().getToken { (done) in
                                if done {
                                    LoginRequests().getFacebookLogin(token: accessToken.tokenString, completionHandler: { (done, userId) in
                                        Session.sharedInstance.userToken = userId
                                        if done {
                                            LoginRequests().finishedProfile(completionHandler: { (done, finished) in
                                                if done && finished {
                                                    ProfileRequests().getProfileInfo { (done, myProfileData) in
                                                        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                                                        if done {
                                                            openNewVC.myProfileData = myProfileData
                                                        } else {
                                                            openNewVC.profileComplete = false
                                                        }
                                                        SwiftLoader.hide()
                                                        DispatchQueue.main.async(execute: { () -> Void in
                                                            self.navigationController?.pushViewController(openNewVC, animated: true)
                                                        });
                                                    }
                                                } else if done && !finished {
                                                    SwiftLoader.hide()
                                                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "TarifSelectionVC") as! TarifSelectionVC
                                                    DispatchQueue.main.async(execute: { () -> Void in
                                                        self.navigationController?.pushViewController(openNewVC, animated: true)
                                                    });
                                                } else {
                                                    SwiftLoader.hide()
                                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                                                }
                                            })
                                        } else {
                                            SwiftLoader.hide()
                                            self._errorAnimeButton()
                                            self.showSimpleAlert(title: "Chyba", message: "Připojení přes Facebook selhalo, opakujte akci později.")
                                        }
                                    })
                                } else {
                                    SwiftLoader.hide()
                                    self._errorAnimeButton()
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                                }
                            }
                        }
                    })
                }
            })
        } else {
            SwiftLoader.hide()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func _errorAnimeButton(){
        simpleLoginBtn.shake(count: 4, for: 0.3, withTranslation: 8)
        registrationBtn.shake(count: 4, for: 0.3, withTranslation: 8)
        UIView.animate(withDuration: 1, animations: {
            self.simpleLoginBtn.backgroundColor = UIColor(red: 220.0/255, green: 46.0/255, blue: 97.0/255, alpha: 1.0)
            self.registrationBtn.backgroundColor = UIColor(red: 220.0/255, green: 46.0/255, blue: 97.0/255, alpha: 1.0)
        }) { (_) in
            UIView.animate(withDuration: 1, animations: {
            }) { (_) in
                UIView.animate(withDuration: 1, animations: {
                    self.simpleLoginBtn.backgroundColor = UIColor(red: 91.0/255, green: 151.0/255, blue: 244.0/255, alpha: 1.0)
                    self.registrationBtn.backgroundColor = UIColor(red: 91.0/255, green: 151.0/255, blue: 244.0/255, alpha: 1.0)
                })
            }
        }
    }
    
    /*
     *  Error occured so log out and return to main screen.
     */
    @objc fileprivate func _serverErr(){
        SwiftLoader.hide()
        let alert = UIAlertController(title: "Chyba", message: "Nastala chyba na straně serveru, opakujte prosím akci později.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            DispatchQueue.main.async(execute: { () -> Void in
                _ = self.navigationController?.popToRootViewController(animated: true)
            })
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
     *  Connection was lost, return to main screen.
     */
    @objc fileprivate func _connectionErr(){
        SwiftLoader.hide()
        let alert = UIAlertController(title: "Bez připojení k internetu", message: "Pro použití aplikace je potřeba internetové připojení.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            DispatchQueue.main.async(execute: { () -> Void in
                _ = self.navigationController?.popToRootViewController(animated: true)
            })
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func _registerObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(self._connectionErr), name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self._serverErr), name: NSNotification.Name(rawValue: "serverErr"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "serverErr"), object: nil)
    }
    
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var emailContainer: UIView!
    @IBOutlet weak var passTxtField: UITextField!
    @IBOutlet weak var passContainer: UIView!
    @IBOutlet weak var simpleLoginBtn: UIButton!
    @IBOutlet weak var facebookLoginBtn: UIView!
    @IBOutlet var facebookRegBtn: UIView!
    @IBOutlet weak var loginContainer: UIView!
    @IBOutlet weak var registrationContainer: UIView!
    @IBOutlet weak var showLoginContainerBtn: UIButton!
    @IBOutlet weak var showRegistrationContinerBtn: UIButton!
    
    //registration
    @IBOutlet weak var registrationBtn: UIButton!
    @IBOutlet weak var regFirstNameContainer: UIView!
    @IBOutlet var regSurnameContainer: UIView!
    @IBOutlet weak var regEmailContainer: UIView!
    @IBOutlet weak var regPassContainer: UIView!
    @IBOutlet var regPassRetypeContainer: UIView!
    @IBOutlet var regFirstNameTxt: UITextField!
    @IBOutlet var regSurnameTxt: UITextField!
    @IBOutlet var regEmailTxt: UITextField!
    @IBOutlet var regPassTxt: UITextField!
    @IBOutlet var regPassRetypeTxt: UITextField!
    @IBOutlet weak var regSwitchbutton: UISwitch!
    
    @IBOutlet weak var regAgreementContainer: UIView!
    @IBOutlet var loginBotConstraint: NSLayoutConstraint!
    @IBOutlet var regBotConstraint: NSLayoutConstraint!
}

extension LoginVC {
    
    @IBAction func showVOPBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "PdfWebVC") as! PdfWebVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }

    @IBAction func showLoginContainerBtn(_ sender: Any) {
        self._loginSceneOn()
    }
    
    @IBAction func showRegistrationContinerBtn(_ sender: Any) {
        self._regiSceneOn()
    }
    
    @IBAction func simpleLoginBtn(_ sender: Any) {
        self._postSimpleLogin()
    }
    
    @IBAction func facebookLoginBtn(_ sender: Any) {
        self._postFacebookLogin()
    }
    
    @IBAction func facebookRegBtn(_ sender: Any) {
        self._postFacebookLogin()
    }
    
    @IBAction func registerBtn(_ sender: Any) {
        self._postRegister()
    }
    
    @IBAction func passResetBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "PasswordResetVC") as! PasswordResetVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
}
