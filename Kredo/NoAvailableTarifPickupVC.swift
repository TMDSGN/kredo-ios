//
//  NoAvailableTarifPickupVC.swift
//  Kredo
//
//  Created by Josef Antoni on 18.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class NoAvailableTarifPickupVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.mainContainer.alpha = 1
            self.view.alpha = 1
        }
    }
    
    fileprivate func _setupScene(){
        self.mainContainer.layer.cornerRadius = 20
        self.backBtn.layer.cornerRadius = 20
        self.mainContainer.alpha = 0
        self.view.alpha = 0
    }
    
    @IBOutlet var mainContainer: UIView!
    @IBOutlet var backBtn: UIButton!
}

extension NoAvailableTarifPickupVC {
    
    @IBAction func backBtn(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.mainContainer.alpha = 0
            self.view.alpha = 0
        }) { (_) in
            self.dismiss(animated: false, completion: nil)
        }
    }
}
