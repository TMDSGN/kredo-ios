//
//  ConsuptionRequests.swift
//  Kredo
//
//  Created by Josef Antoni on 14.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct TarifDetailData {
    
    var name, info, freeMinAll, freeMinOwn, priceCallAll, priceCallOwn, freeSmsAll, freeSmsOwn, priceSmsAll, priceSmsOwn, dataSpeed, freeData, wifiData, wifiSpeed, tvChannels, tvDevices, cross, crossType: String
    
    init(name: String, info: String, freeMinAll: String, freeMinOwn: String, priceCallAll: String, priceCallOwn: String, freeSmsAll: String, freeSmsOwn: String, priceSmsAll: String, priceSmsOwn: String, dataSpeed: String, freeData: String, wifiData: String, wifiSpeed: String, tvChannels: String, tvDevices: String, cross: String, crossType: String) {
        self.name = name
        self.info = info
        self.freeMinAll = freeMinAll
        self.freeMinOwn = freeMinOwn
        self.priceCallAll = priceCallAll
        self.priceCallOwn = priceCallOwn
        self.freeSmsAll = freeSmsAll
        self.freeSmsOwn = freeSmsOwn
        self.priceSmsAll = priceSmsAll
        self.priceSmsOwn = priceSmsOwn
        self.dataSpeed = dataSpeed
        self.freeData = freeData
        
        self.wifiData = wifiData
        self.wifiSpeed = wifiSpeed
        self.tvChannels = tvChannels
        self.tvDevices = tvDevices
        self.cross = cross
        self.crossType = crossType
    }
}

struct OffersData {
    
    var myMinutes, mySms, myData, myCover: String
    var idArr, carrierArr, nameArr, monthlySpendArr, minutesArr, smsArr, dataArr, coverArr, cheaperArr, crossArr: [String]
    var notAvailableTarifArr: [Bool]
    
    init(myMinutes: String, mySms: String, myData: String, myCover: String, idArr: [String], carrierArr: [String], nameArr: [String], monthlySpendArr: [String], minutesArr: [String], smsArr: [String], dataArr: [String], coverArr: [String], cheaperArr: [String], crossArr: [String], notAvailableTarifArr: [Bool]) {
        self.myMinutes = myMinutes
        self.mySms = mySms
        self.myData = myData
        self.myCover = myCover
        self.idArr = idArr
        self.carrierArr = carrierArr
        self.nameArr = nameArr
        self.monthlySpendArr = monthlySpendArr
        self.minutesArr = minutesArr
        self.smsArr = smsArr
        self.dataArr = dataArr
        self.coverArr = coverArr
        self.cheaperArr = cheaperArr
        self.crossArr = crossArr
        self.notAvailableTarifArr = notAvailableTarifArr
    }
}

struct CarrierData {
    
    var idArr: [Int]
    var nameArr: [String]
    
    init(idArr: [Int], nameArr: [String]) {
        self.idArr = idArr
        self.nameArr = nameArr
    }
}

class ConsuptionRequests {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func putUserInfo(minutes: Double, sms: Double, data: Double, carrier: Int, commonLocations: [String], monthlySpend: Double, individual: Bool, born: String, ico: String, address: String, contact: String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/consumption/v1/consumption_info"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        
        var para = [
            "minutes" : minutes,
            "sms" : sms,
            "data" : data,
            "carrier" : carrier,
            "common_locations" : commonLocations,
            "monthly_spend" : monthlySpend,
            "individual" : individual,
            "born" : born
            ] as [String : Any]
        
        if !individual {
            para = [
                "minutes" : minutes,
                "sms" : sms,
                "data" : data,
                "carrier" : carrier,
                "common_locations" : commonLocations,
                "monthly_spend" : monthlySpend,
                "individual" : individual,
                "ico" : ico,
                "address" : address,
                "contact" : contact
            ]
        }
        Alamofire.request(url, method: .put, parameters:para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 202 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func doUserHaveTarif(gotTarif: Bool, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/consumption/v1/tarif_info"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        let para = [
            "got_tarif" : gotTarif
            ] as [String : Any]
        Alamofire.request(url, method: .put, parameters:para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 202 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func howManyPhoneNumbersUserNeed(phoneNumbers: Int, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/consumption/v1/phone_numbers_count"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        let para = [
            "phone_numbers" : phoneNumbers
            ] as [String : Any]
        Alamofire.request(url, method: .put, parameters:para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 202 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func getSumarizationInfo(completionHandler: @escaping (Bool, String, String, String, String, String, String, String, String) -> ()) -> (){
        
        let url = urlDomain + "/consumption/v1/summarization_info"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.description)
            if response.response?.statusCode == 202 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let minutes = json["minutes"].stringValue
                    let sms = json["sms"].stringValue
                    let data = json["data"].stringValue
                    let monthlySpend = json["monthly_spend"].stringValue
                    let cheaper = json["cheaper"].stringValue
                    let infoText = json["info_text"].stringValue
                    let bonus = json["bonus"].stringValue
                    let bonusForSharing = json["bonus_for_sharing"].stringValue

                    completionHandler(true, minutes, sms, data, monthlySpend, cheaper, infoText, bonus, bonusForSharing)
                } else {
                    completionHandler(false, String(), String(), String(), String(), String(), String(), String(), String())
                }
            } else {
                completionHandler(false, String(), String(), String(), String(), String(), String(), String(), String())
            }
        }
    }
    
    func getListOfOffers(completionHandler: @escaping (Bool, OffersData?) -> ()) -> (){
        
        let url = urlDomain + "/tarifs/v1/tarif_offers"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            print(response.debugDescription)
            if response.response?.statusCode == 200 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let minutes = json["minutes"].stringValue
                    let sms = json["sms"].stringValue
                    let data = json["data"].stringValue
                    let cover = json["cover"].stringValue
                    
                    var idArr: [String] = []
                    var carrierArr: [String] = []
                    var nameArr: [String] = []
                    var monthlySpendArr: [String] = []
                    var minutesArr: [String] = []
                    var smsArr: [String] = []
                    var dataArr: [String] = []
                    var coverArr: [String] = []
                    var cheaperArr: [String] = []
                    var notAvailableTarifArr: [Bool] = []
                    var crossArr: [String] = []
                    
                    for i in 0..<json["list"].count {
                        let id = json["list"][i]["id"].stringValue
                        let carrier = json["list"][i]["carrier"].stringValue
                        let name = json["list"][i]["name"].stringValue
                        let monthlySpend = json["list"][i]["monthly_spend"].stringValue
                        let minutes = json["list"][i]["minutes"].stringValue
                        let sms = json["list"][i]["sms"].stringValue
                        let data = json["list"][i]["data"].stringValue
                        let cover = json["list"][i]["cover"].stringValue
                        let cheaper = json["list"][i]["cheaper"].stringValue
                        let notAvailableTarif = json["list"][i]["red_button"].boolValue
                        let cross = json["list"][i]["cross"].stringValue
                        
                        idArr.append(id)
                        carrierArr.append(carrier)
                        nameArr.append(name)
                        monthlySpendArr.append(monthlySpend)
                        minutesArr.append(minutes)
                        smsArr.append(sms)
                        dataArr.append(data)
                        coverArr.append(cover)
                        cheaperArr.append(cheaper)
                        notAvailableTarifArr.append(notAvailableTarif)
                        crossArr.append(cross)
                    }
                    let offerData: OffersData = OffersData(myMinutes: minutes, mySms: sms, myData: data, myCover: cover, idArr: idArr, carrierArr: carrierArr, nameArr: nameArr, monthlySpendArr: monthlySpendArr, minutesArr: minutesArr, smsArr: smsArr, dataArr: dataArr, coverArr: coverArr, cheaperArr: cheaperArr, crossArr: crossArr, notAvailableTarifArr: notAvailableTarifArr)
                    completionHandler(true, offerData)
                } else {
                    completionHandler(false, nil)
                }
            } else {
                completionHandler(false, nil)
            }
        }
    }
    
    func getTarifDetail(id: String, completionHandler: @escaping (Bool, TarifDetailData?) -> ()) -> (){
        
        let url = urlDomain + "/tarifs/v1/tarif_offers/\(id)"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let name = json["name"].stringValue
                    let info = json["info"].stringValue
                    let freeMinAll = json["free_min_all"].stringValue
                    let freeMinOwn = json["free_min_own"].stringValue
                    let priceCallAll = json["price_call_all"].stringValue
                    let priceCallOwn = json["price_call_own"].stringValue
                    let freeSmsAll = json["free_sms_all"].stringValue
                    let freeSmsOwn = json["free_sms_own"].stringValue
                    let priceSmsAll = json["price_sms_all"].stringValue
                    let priceSmsOwn = json["price_sms_own"].stringValue
                    let dataSpeed = json["data_speed"].stringValue
                    let freeData = json["free_data"].stringValue
                    
                    let wifiData = json["wifi_data"].stringValue
                    let wifiSpeed = json["wifi_speed"].stringValue
                    let tvChannels = json["tv_channels"].stringValue
                    let tvDevices = json["tv_devices"].stringValue
                    let cross = json["cross"].stringValue
                    let crossType = json["cross_type"].stringValue
                    
                    let detailData = TarifDetailData(name: name, info: info, freeMinAll: freeMinAll, freeMinOwn: freeMinOwn, priceCallAll: priceCallAll, priceCallOwn: priceCallOwn, freeSmsAll: freeSmsAll, freeSmsOwn: freeSmsOwn, priceSmsAll: priceSmsAll, priceSmsOwn: priceSmsOwn, dataSpeed: dataSpeed, freeData: freeData, wifiData: wifiData, wifiSpeed: wifiSpeed, tvChannels: tvChannels, tvDevices: tvDevices, cross: cross, crossType: crossType)
                    
                    completionHandler(true, detailData)
                } else {
                    completionHandler(false, nil)
                }
            } else {
                completionHandler(false, nil)
            }
        }
    }
    
    func postTarifOrder(id: String){
        
        let url = urlDomain + "/tarifs/v1/tarif/\(id)"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
        }
    }
    
    func getCarriers(completionHandler: @escaping (Bool, CarrierData?) -> ()) -> (){
        
        let url = urlDomain + "/consumption/v1/carriers"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let data = response.result.value {
                    let json = JSON(data)
                    print(json)
                    var idArr: [Int] = []
                    var nameArr: [String] = []
                    for i in 0..<json["carriersList"].count {
                        let id = json["carriersList"][i]["id"].intValue
                        let name = json["carriersList"][i]["name"].stringValue.replacingOccurrences(of: " - CZ", with: "").replacingOccurrences(of: " CZ", with: "")
                        idArr.append(id)
                        nameArr.append(name)
                    }
                    let carrierData: CarrierData = CarrierData(idArr: idArr, nameArr: nameArr)
                    completionHandler(true, carrierData)
                } else {
                    completionHandler(false, nil)
                }
            } else {
                completionHandler(false, nil)
            }
        }
    }
}
