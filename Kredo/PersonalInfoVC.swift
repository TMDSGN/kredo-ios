//
//  PersonalInfoVC.swift
//  Kredo
//
//  Created by Josef Antoni on 15.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class PersonalInfoVC: UIViewController, AlertMessage {
    
    fileprivate var _isSwitcherOn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
        _getPersonalInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        _registerObservers()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        if Session.sharedInstance.userHaveProfile {
            coverContainer.isHidden = true
            userHaveTaritConstraint.isActive = true
            userHaveNotTarifConstraint.isActive = false
        } else {
            coverContainer.isHidden = false
            userHaveTaritConstraint.isActive = false
            userHaveNotTarifConstraint.isActive = true
        }
    }
    
    fileprivate func _getPersonalInfo(){
        if Reachability.isConnectedToNetwork() {
            ProfileRequests().getPersonalInfo { (done, firstName, lastName, email, phoneNumber) in
                self.nameTxtField.text! = firstName
                self.surnameTxtField.text! = lastName
                self.mailTxtField.text! = email
                self.phoneTxtField.text! = phoneNumber
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func _sendPersonalInfo(){
        if Reachability.isConnectedToNetwork() {
            if !phoneTxtField.text!.isEmpty {
                
                if phoneTxtField.text!.characters.count > 8 {
                    if !mailTxtField.text!.isEmpty {
                        if !nameTxtField.text!.isEmpty {
                            if !surnameTxtField.text!.isEmpty {
                                if cvopTxtField.text!.characters.count < 15 {
                                SwiftLoader.show(animated: true)
                                ProfileRequests().putPersonalInfo(persistNumber: _isSwitcherOn, cvop: cvopTxtField.text!, firstName: nameTxtField.text!, lastName: surnameTxtField.text!, email: mailTxtField.text!, phoneNumber: phoneTxtField.text!, contact_me: contactCalendarLabel.text!, ending: kontraktEndLabel.text!, completionHandler: { (done) in
                                    SwiftLoader.hide()
                                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ContractCreatedVC") as! ContractCreatedVC
                                    DispatchQueue.main.async(execute: { () -> Void in
                                        self.navigationController?.pushViewController(openNewVC, animated: true)
                                    });
                                })
                                } else {
                                    cvopContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                    _errorAnimeButton()
                                }
                            } else {
                                surnameContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                _errorAnimeButton()
                            }
                        } else {
                            firstnameContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                            _errorAnimeButton()
                        }
                    } else {
                        mailContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                        _errorAnimeButton()
                    }
                } else {
                    showSimpleAlert(title: "Neplatné telefonní číslo", message: "Formát telefonního čísla není správný, prosím překontrolujte si vložené číslo.")
                    _errorAnimeButton()
                }
            } else {
                phoneContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                _errorAnimeButton()
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func _errorAnimeButton(){
        sendBtn.shake(count: 4, for: 0.3, withTranslation: 8)
        UIView.animate(withDuration: 1, animations: {
            self.sendBtn.backgroundColor = UIColor(red: 220.0/255, green: 46.0/255, blue: 97.0/255, alpha: 1.0)
        }) { (_) in
            UIView.animate(withDuration: 1, animations: {
            }) { (_) in
                UIView.animate(withDuration: 1, animations: {
                    self.sendBtn.backgroundColor = UIColor(red: 91.0/255, green: 151.0/255, blue: 244.0/255, alpha: 1.0)
                })
            }
        }
    }
    
    fileprivate func _setupScene(){
        self.cvopContainer.layer.cornerRadius = 20
        self.cvopContainer.layer.shadowOpacity = 0.7
        self.cvopContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.cvopContainer.layer.shadowRadius = 8
        self.cvopContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        
        self.phoneContainer.layer.cornerRadius = 20
        self.phoneContainer.layer.shadowOpacity = 0.7
        self.phoneContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.phoneContainer.layer.shadowRadius = 8
        self.phoneContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        
        self.mailContainer.layer.cornerRadius = 20
        self.mailContainer.layer.shadowOpacity = 0.7
        self.mailContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.mailContainer.layer.shadowRadius = 8
        self.mailContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        
        self.firstnameContainer.layer.cornerRadius = 20
        self.firstnameContainer.layer.shadowOpacity = 0.7
        self.firstnameContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.firstnameContainer.layer.shadowRadius = 8
        self.firstnameContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        
        self.surnameContainer.layer.cornerRadius = 20
        self.surnameContainer.layer.shadowOpacity = 0.7
        self.surnameContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.surnameContainer.layer.shadowRadius = 8
        self.surnameContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        
        self.calendarContainer.layer.cornerRadius = 20
        self.calendarContainer.layer.shadowOpacity = 0.7
        self.calendarContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.calendarContainer.layer.shadowRadius = 8
        self.calendarContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        
        self.kontraktEndContainer.layer.cornerRadius = 20
        self.kontraktEndContainer.layer.shadowOpacity = 0.7
        self.kontraktEndContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.kontraktEndContainer.layer.shadowRadius = 8
        self.kontraktEndContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        
        self.sendBtn.layer.cornerRadius = 20
        self.sendBtn.layer.shadowOpacity = 0.7
        self.sendBtn.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.sendBtn.layer.shadowRadius = 8
        self.sendBtn.layer.shadowColor = UIColor(red: 91/255.0, green: 151/255.0, blue: 244/255.0, alpha: 0.2).cgColor
        
        //red dots
        self.requiredDot.layer.cornerRadius = 3
        self.phoneDot.layer.cornerRadius = 3
        self.emailDot.layer.cornerRadius = 3
        self.firstNameDot.layer.cornerRadius = 3
        self.secondNameDot.layer.cornerRadius = 3
        
        //switcher
        self.switcherContainer.layer.borderWidth = 0.5
        self.switcherContainer.layer.borderColor = UIColor(red: 91/255.0, green: 151/255.0, blue: 244/255.0, alpha: 1.0).cgColor
        self.switcherContainer.layer.cornerRadius = self.switcherContainer.frame.size.height/2
        self.switcherDot.backgroundColor = UIColor(red: 91/255.0, green: 151/255.0, blue: 244/255.0, alpha: 1.0)
        self.switcherDot.layer.cornerRadius = self.switcherDot.frame.size.height/2
        self.switcherDot.alpha = 0
    }
    
    @objc fileprivate func postDate(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let selectedDate = dict["selectedDate"] as! String
        let kontraktEndSelected = dict["kontraktEndSelected"] as! Bool
        if kontraktEndSelected {
            self.kontraktEndLabel.text = selectedDate
        } else {
            self.contactCalendarLabel.text = selectedDate
        }
    }
    
    fileprivate func _getProfileInfo(){
        if Reachability.isConnectedToNetwork() {
            ProfileRequests().getProfileInfo { (done, myProfileData) in
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                if done {
                    openNewVC.myProfileData = myProfileData
                } else {
                    openNewVC.profileComplete = false
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func _registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.postDate), name: NSNotification.Name(rawValue: "postDatePersonalInfo"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "postDatePersonalInfo"), object: nil)
    }
    
    @IBOutlet weak var cvopContainer: UIView!
    @IBOutlet weak var phoneContainer: UIView!
    @IBOutlet weak var mailContainer: UIView!
    @IBOutlet weak var firstnameContainer: UIView!
    @IBOutlet weak var surnameContainer: UIView!
    @IBOutlet weak var calendarContainer: UIView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet var kontraktEndContainer: UIView!
    
    @IBOutlet var phoneTxtField: UITextField!
    @IBOutlet var mailTxtField: UITextField!
    @IBOutlet var nameTxtField: UITextField!
    @IBOutlet var surnameTxtField: UITextField!
    @IBOutlet var cvopTxtField: UITextField!
    @IBOutlet var contactCalendarLabel: UITextField!
    @IBOutlet var kontraktEndLabel: UITextField!
    
    //red dots
    @IBOutlet weak var requiredDot: UIView!
    @IBOutlet weak var phoneDot: UIView!
    @IBOutlet weak var emailDot: UIView!
    @IBOutlet weak var firstNameDot: UIView!
    @IBOutlet weak var secondNameDot: UIView!
    
    //switcher
    @IBOutlet weak var switcherContainer: UIView!
    @IBOutlet weak var switcherDot: UIView!
    
    @IBOutlet var userHaveNotTarifConstraint: NSLayoutConstraint!
    @IBOutlet var userHaveTaritConstraint: NSLayoutConstraint!
    @IBOutlet var coverContainer: UIView!
}
extension PersonalInfoVC {
    
    @IBAction func cvopBtn(_ sender: Any) {
        self.showSimpleAlert(title: "", message: "ČVOP je čtrnáctimístný kód, který je určený k přenosu čísla. Je platný 60 dní od data vydání a obvykle vám jej operátor zašle  SMS zprávou na vyžádání. Viz kredo.online.")
    }
    
    @IBAction func calendarBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        modal.personalInfoDate = true
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
    
    @IBAction func kontraktEndBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        modal.personalInfoDate = false
        modal.kontraktEndView = true
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
    
    @IBAction func openProfileBtn(_ sender: Any) {
        _getProfileInfo()
    }
    
    @IBAction func switcherPressedBtn(_ sender: Any) {
        if _isSwitcherOn {
            _isSwitcherOn = false
            UIView.animate(withDuration: 1, animations: {
                self.switcherDot.alpha = 0
            })
        } else {
            _isSwitcherOn = true
            UIView.animate(withDuration: 1, animations: {
                self.switcherDot.alpha = 1
            })
        }
    }
    
    @IBAction func sendBtn(_ sender: Any) {
        _sendPersonalInfo()
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}
