//
//  CityData.swift
//  Kredo
//
//  Created by Josef Antoni on 21.09.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import RealmSwift

class CityData {
    
    static let sharedInstance = CityData()
    
    var datasource:Results<Pokryti>!
    
    func getItems(_ filter:String){
        let conf = Realm.Configuration(
            fileURL: Bundle.main.url(forResource: "Pokryti", withExtension: "realm"),
            readOnly: true)

        do {
            let realm = try Realm(configuration: conf)
            if filter.isEmpty {
                datasource = realm.objects(Pokryti.self).sorted(byKeyPath: "Mesto")
            } else {
                datasource = realm.objects(Pokryti.self).filter(filter).sorted(byKeyPath: "Mesto")
            }
            print(datasource)
        } catch let error as NSError {
            print(error)
        }
    }
}
