//
//  PromoCodeVC.swift
//  Kredo
//
//  Created by Josef Antoni on 04.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class PromoCodeVC: UIViewController, AlertMessage {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
    }
    
    fileprivate func _setupScene(){
        self.navigationController?.isNavigationBarHidden = true
        self.promoCodeContainer.layer.cornerRadius = self.promoCodeContainer.frame.height/2
    }
    
    fileprivate func _closeView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var promoCodeContainer: UIView!
    @IBOutlet weak var promoCodeTxtField: UITextField!
    
}

extension PromoCodeVC {
    
    @IBAction func enterPromoCode(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            if !promoCodeTxtField.text!.isEmpty {
                LoginRequests().registerPromocode(promoCode: promoCodeTxtField.text!) { (done) in
                    self._closeView()
                }
            } else {
                promoCodeContainer.shake(count: 4, for: 0.3, withTranslation: 8)
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @IBAction func noPromoCode(_ sender: Any) {
        _closeView()
    }
}
