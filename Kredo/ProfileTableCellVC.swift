//
//  ProfileTableRowVC.swift
//  Kredo
//
//  Created by Josef Antoni on 08.09.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class ProfileTableCellVC: UITableViewCell {

    @IBOutlet var promoIcon: UIImageView!
    @IBOutlet var promoName: UILabel!
    
}
