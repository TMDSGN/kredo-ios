//
//  DelayFunc.swift
//  Kredo
//
//  Created by Josef Antoni on 12.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation

class DelayFunc {
    static let sharedInstance = DelayFunc()
    fileprivate init() {}//This prevents others from using the default '()' initializer for this class.
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
}
