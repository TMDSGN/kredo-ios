//
//  ProfileRequests.swift
//  Kredo
//
//  Created by Josef Antoni on 17.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct MyProfileData {
    
    var email, firstName, lastName, kredits, newCost, newData, newMin, newSms, oldCost, oldData, oldMin, oldSms, phoneNumber, photo, promoCode, promoUsed, saved, shareLink: String
    var promoAvatarArr, promoNameArr: [String]
    var finishedAccount, buttonShow: Bool
    
    init(finishedAccount: Bool, email: String, firstName: String, lastName: String, kredits: String, newCost: String, newData: String, newMin: String, newSms: String, oldCost: String, oldData: String, oldMin: String, oldSms: String, phoneNumber: String, photo: String, promoCode: String, promoUsed: String, saved: String, promoAvatarArr: [String], promoNameArr: [String], buttonShow: Bool, shareLink: String) {
        self.finishedAccount = finishedAccount
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.kredits = kredits
        self.newCost = newCost
        self.newData = newData
        self.newMin = newMin
        self.newSms = newSms
        self.oldCost = oldCost
        self.oldData = oldData
        self.oldMin = oldMin
        self.oldSms = oldSms
        self.phoneNumber = phoneNumber
        self.photo = photo
        self.promoCode = promoCode
        self.promoUsed = promoUsed
        self.saved = saved
        self.promoAvatarArr = promoAvatarArr
        self.promoNameArr = promoNameArr
        self.buttonShow = buttonShow
        self.shareLink = shareLink
    }
}

class ProfileRequests {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getPersonalInfo(completionHandler: @escaping (Bool, String, String, String, String) -> ()) -> (){
        
        let url = urlDomain + "/profile/v1/personal_info"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 202 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let firstName = json["first_name"].stringValue
                    let lastName = json["last_name"].stringValue
                    let email = json["email"].stringValue
                    let phoneNumber = json["phone_number"].stringValue
                    completionHandler(true, firstName, lastName, email, phoneNumber)
                } else {
                    completionHandler(false, "", "", "", "")
                }
            } else {
                completionHandler(false, "", "", "", "")
            }
        }
    }
    
    func putPersonalInfo(persistNumber: Bool, cvop: String, firstName: String, lastName: String, email: String, phoneNumber: String, contact_me: String, ending: String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/profile/v1/personal_info_more"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        
        let para = [
            "persist_number" : persistNumber,
            "cvop" : cvop,
            "first_name" : firstName,
            "last_name" : lastName,
            "email" : email,
            "phone_number" : phoneNumber,
            "contact_me" : contact_me,
            "ending" : ending
            ] as [String : Any]
        
        Alamofire.request(url, method: .put, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 202 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func getProfileInfo(completionHandler: @escaping (Bool, MyProfileData?) -> ()) -> (){
        
        let url = urlDomain + "/profile/v1/my_profile"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 202 {
                if let data = response.result.value {
                    let json = JSON(data)
                    print(data)
                    let finishedAccount = json["finished"].boolValue

                    let email = json["email"].stringValue
                    let firstName = json["first_name"].stringValue
                    let lastName = json["last_name"].stringValue
                    let kredits = json["kredits"].stringValue
                    let newCost = json["new_cost"].stringValue
                    let newData = json["new_data"].stringValue
                    let newMin = json["new_min"].stringValue
                    let newSms = json["new_sms"].stringValue
                    let oldCost = json["old_cost"].stringValue
                    let oldData = json["old_data"].stringValue
                    let oldMin = json["old_min"].stringValue
                    let oldSms = json["old_sms"].stringValue
                    let phoneNumber = json["phone_number"].stringValue
                    let photo = json["photo"].stringValue
                    let promoCode = json["promo_code"].stringValue
                    let promoUsed = json["promo_used"].stringValue
                    let saved = json["saved"].stringValue
                    let buttonShow = json["button_show"].boolValue
                    let shareLink = json["share_link"].stringValue

                    var promoAvatarArr: [String] = []
                    var promoNameArr: [String] = []
                    
                    for i in 0..<json["promo_array"].count {
                        let promoAvatar = json["promo_array"][i]["avatar"].stringValue
                        let promoName = json["promo_array"][i]["name"].stringValue
                        promoAvatarArr.append(promoAvatar)
                        promoNameArr.append(promoName)
                    }
                    
                    completionHandler(true, MyProfileData(finishedAccount: finishedAccount, email: email, firstName: firstName, lastName: lastName, kredits: kredits, newCost: newCost, newData: newData, newMin: newMin, newSms: newSms, oldCost: oldCost, oldData: oldData, oldMin: oldMin, oldSms: oldSms, phoneNumber: phoneNumber, photo: photo, promoCode: promoCode, promoUsed: promoUsed, saved: saved, promoAvatarArr: promoAvatarArr, promoNameArr: promoNameArr, buttonShow: buttonShow, shareLink: shareLink))
                } else {
                    completionHandler(false, nil)
                }
            } else {
                completionHandler(false, nil)
            }
        }
    }
    
    func sentPromoCode(promoCode:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/promo_code"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "promo_code" : promoCode
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
            if response.response?.statusCode == 202 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func updateNewPhoto(photo:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/profile/v1/photo"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        let para = [
            "photo" : photo
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
            if response.response?.statusCode == 204 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func sendBankAccountNumber(number:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/profile/v1/account_number"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        let para = [
            "account_number" : number
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
            if response.response == nil {
                 completionHandler(true)
            } else {
                completionHandler(false)
            }
    
          
        }
    }}
