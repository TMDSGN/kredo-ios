//
//  AlertMessage.swift
//  Kredo
//
//  Created by Josef Antoni on 12.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

protocol AlertMessage {}

extension AlertMessage where Self: UIViewController {
    /*
     * Show simple info alert with OK button.
     */
    func showSimpleAlert( title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
