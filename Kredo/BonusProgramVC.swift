//
//  BonusProgramVC.swift
//  Kredo
//
//  Created by Josef Antoni on 23.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class BonusProgramVC: UIViewController {
    
    var monthBonus: String?
    var bonusForSharing: String?
    
    override func viewDidLoad() {
        self.continueContainer.cornerRadius = self.continueContainer.frame.height/2
        
        if let bonusStr = monthBonus {
            if let bonusInt = Int(bonusStr) {
            
                if let bonusForSharingVal = bonusForSharing {
                    
                        var bfsFloat = (bonusForSharingVal as NSString).doubleValue
                        let bfsRounded : Double = bfsFloat.rounded()
                        let bfsInt : Int = Int(bfsRounded)
                        
                        self.betterPriceForUserLabel.text = "\(bonusInt) Kč"
                        self.rightBonusPriceLabel.text = "\(bfsInt) Kč"
                        self.centerBonusPriceLabel.text = "\(bfsInt) Kč"
                        self.leftBonusPriceLabel.text = "\(bfsInt) Kč"
                        self.totalBetterPriceLabel.text = "\(bonusInt + 3 * bfsInt) Kč"
                
                }
            }
        }
        
    }
    
    fileprivate func _getProfileInfo() {
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileRequests().getProfileInfo { (done, myProfileData) in
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                if done {
                    openNewVC.myProfileData = myProfileData
                } else {
                    openNewVC.profileComplete = false
                }
                SwiftLoader.hide()
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }

    
    @IBOutlet var rightBonusPriceLabel: UILabel!
    @IBOutlet var centerBonusPriceLabel: UILabel!
    @IBOutlet var leftBonusPriceLabel: UILabel!
    @IBOutlet var betterPriceForUserLabel: UILabel!
    @IBOutlet var totalBetterPriceLabel: UILabel!
    @IBOutlet var continueContainer: UIView!
}

extension BonusProgramVC {
    
    @IBAction func continueBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "OffersVC") as! OffersVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }

    @IBAction func profileBtn(_ sender: Any) {
        _getProfileInfo()
    }
}
