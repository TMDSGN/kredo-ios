//
//  SumarizationVC.swift
//  Kredo
//
//  Created by Josef Antoni on 11.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class SumarizationVC: UIViewController {
    
    var minutes: String = ""
    var sms: String = ""
    var data: String = ""
    var monthlySpend: String = ""
    var cheaper: String = ""
    var bonus: String = ""
    var bonusForSharing: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    fileprivate func _setupScene(){
        self.minLabel.text = minutes + " min."
        self.smsLabel.text = sms + " sms"
        self.dataLabel.text = data + " GB"
        self.priceLabel.text = monthlySpend + " Kč"
        self.betterPriceLabel.text = " i víc až o \(cheaper) Kč"
        
        if let cheaperint = Int(cheaper) {
            if cheaperint < 0 {
                self.firstBlueTitle.text = "Pro řešení tvého požadavku se nabízí jedna z neveřejných nabídek."
                //self.betterPriceLabel.text = "o \(cheaper.replacingOccurrences(of: "-", with: "")) Kč"
                self.betterPriceLabel.isEnabled = false
                self.betterPriceLabel.isHidden = true
                self.thirdBlueTitle.font = self.thirdBlueTitle.font.withSize(14)
                self.thirdBlueTitle.text = "Pro její sdělení dokonči proces v aplikaci. Vyber si jeden z našich veřejných tarifů co splňuje tvé požadavky a odešli nezávaznou objednávku. Pak už jen čekej na telefonát kde se dozvíš víc."
            }
        }
        let labelFont = UIFont(name: "HelveticaNeue-Medium", size: 17)
        let labelColor = UIColor(red: 129.0/255, green: 129.0/255, blue: 129.0/255, alpha: 1)
        let labelUnderline = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let attributes: [String:AnyObject] = [NSFontAttributeName : labelFont!, NSForegroundColorAttributeName: labelColor]
        let attrString = NSAttributedString(string:"Navíc při sjednání dostaneš od nás bonus ve výši \(bonus)Kč a při účasti v Benefitním programu můžeš mít svůj nový tarif celý zadarmo. Více ", attributes: attributes)
        let underlineAttributedString = NSMutableAttributedString(string: "zde.", attributes: attributes)
        let textRange = NSMakeRange(0, 3)
        underlineAttributedString.addAttributes(labelUnderline, range: textRange)
        //combine both strings
        let combination = NSMutableAttributedString()
        combination.append(attrString)
        combination.append(underlineAttributedString)
        self.informationLabel.attributedText = combination

        //rounded corners
        self.noInterestedBtn.layer.cornerRadius = 20
        self.interestedBtn.layer.cornerRadius = 20
        //add shadow
        self.noInterestedBtn.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.interestedBtn.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
    }
    
    fileprivate func _getProfileInfo(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileRequests().getProfileInfo { (done, myProfileData) in
                SwiftLoader.hide()
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                if done {
                    openNewVC.myProfileData = myProfileData
                } else {
                    openNewVC.profileComplete = false
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @IBOutlet var minLabel: UILabel!
    @IBOutlet var smsLabel: UILabel!
    @IBOutlet var dataLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var informationLabel: UILabel!
    @IBOutlet weak var noInterestedBtn: UIButton!
    @IBOutlet weak var interestedBtn: UIButton!
    
    @IBOutlet var firstBlueTitle: UILabel!
    @IBOutlet var betterPriceLabel: UILabel!
    @IBOutlet var thirdBlueTitle: UILabel!
}

extension SumarizationVC {
    
    @IBAction func moreInfoBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "PdfWebVC") as! PdfWebVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }

    @IBAction func openProfileBtn(_ sender: Any) {
        _getProfileInfo()
    }
    
    @IBAction func interestedBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "BonusProgramVC") as! BonusProgramVC
        openNewVC.monthBonus = bonus
        openNewVC.bonusForSharing = bonusForSharing
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func noInterestBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "NoInterestVC") as! NoInterestVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}
