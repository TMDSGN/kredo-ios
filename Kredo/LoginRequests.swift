//
//  LoginRequests.swift
//  Kredo
//
//  Created by Josef Antoni on 10.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class LoginRequests {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func simpleLogin(email:String, pass:String, completionHandler: @escaping (Bool, String?) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/login"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "email" : email,
            "password" : pass
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
            if response.response?.statusCode == 201 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let userId = json["user_id"].stringValue
                    Session.sharedInstance.userToken = userId
                    completionHandler(true, nil)
                } else {
                    completionHandler(false, nil)
                }
            } else if response.response?.statusCode == 401 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let invalidEmail = json["errors"]["invalid_email"][0].stringValue
                    let invalidUsername = json["errors"]["invalid_username"][0].stringValue
                    let invalidPassword = json["errors"]["incorrect_password"][0].stringValue
                    if !invalidEmail.isEmpty || !invalidUsername.isEmpty {
                        completionHandler(false, "Emailová adresa není platná nebo neexistuje.")
                    }
                    if !invalidPassword.isEmpty {
                        completionHandler(false, "Zadané heslo není správné, zkuste to znovu.")
                    }
                    completionHandler(false, nil)
                } else {
                    completionHandler(false, nil)
                }
            } else {
                completionHandler(false, nil)
            }
        }
    }
    
    func registration(firstName: String, lastName: String, email: String, pass: String, completionHandler: @escaping (Bool, String) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/register"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "email" : email,
            "password" : pass,
            "first_name" : firstName,
            "last_name" : lastName
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
            if response.response?.statusCode == 201 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let userId = json["user_id"].stringValue
                    Session.sharedInstance.userToken = userId
                    if !userId.isEmpty {
                        completionHandler(true, "")
                    } else {
                        completionHandler(false, "Překontrolujte vložené údaje a opakujte akci později.")
                    }
                } else {
                    completionHandler(false, "Překontrolujte vložené údaje a opakujte akci později.")
                }
            } else if response.response?.statusCode == 403 {
                completionHandler(false, "Zvolená emailová adresa je již použitá. Zvolte prosím jinou.")
            } else {
                completionHandler(false, "Překontrolujte vložené údaje a opakujte akci později.")
            }
        }
    }
    
    func getFacebookLogin(token:String, completionHandler: @escaping (Bool, String) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/facebook"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "facebook_token" : token]
        
        Alamofire.request( url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let token = json["user_id"].stringValue
                    if !token.isEmpty {
                        Session.sharedInstance.facebookUser = true
                        completionHandler(true, token)
                    } else {
                        completionHandler(false, String())
                    }
                } else {
                    completionHandler(false, String())
                }
            } else {
                completionHandler(false, String())
            }
        }
    }
    
    func conectPhoneNumberToAcc(phoneNumber: String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/phone_number"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        let para = [
            "phone_number" : phoneNumber
        ]
        Alamofire.request(url, method: .put, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 202 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func registerPromocode(promoCode: String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/promo_code"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        let para = [
            "promo_code" : promoCode
        ]
        
        Alamofire.request(url, method: .put, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
            if response.response?.statusCode == 202 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func finishedProfile(completionHandler: @escaping (Bool, Bool) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/finished"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
            if response.response?.statusCode == 200 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let finished = json["finished"].boolValue
                    completionHandler(true, finished)
                } else {
                    completionHandler(false, false)
                }
            } else {
                completionHandler(false, false)
            }
        }
    }
    
    
    func resetPass(email: String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/forgotten_password"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "email" : email
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
            if (response.response?.statusCode == nil) || (response.response?.statusCode == 204) {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func deleteUser(completionHandler: @escaping (Bool, String?) -> ()) -> (){
        
        let url = urlDomain + "/profile/v1/my_profile/delete"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "User-Id": Session.sharedInstance.userToken
        ]
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response.debugDescription)
            if (response.response?.statusCode == nil) || (response.response?.statusCode == 204) {
                completionHandler(true, nil)
            } else {
                completionHandler(false, nil)
            }
        }
    }
}
