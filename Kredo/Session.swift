//
//  Session.swift
//  Kredo
//
//  Created by Josef Antoni on 10.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
class Session {
    
    static let sharedInstance = Session()
    fileprivate init(){//This prevents others from using the default '()' initializer for this class.
    }
    
    var securityToken: String = ""
    var userToken: String = ""
    var urlDomain: String = "https://kredo.online/api"
    var facebookUser = false
    var carrierData: CarrierData?
    var userHaveProfile = true
}
