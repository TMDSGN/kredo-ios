//
//  TarifSelectionVC.swift
//  Kredo
//
//  Created by Josef Antoni on 10.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class TarifSelectionVC: UIViewController {
    
    fileprivate var _seenPromoScreen = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        if !_seenPromoScreen {
            _presentModal()
        }
    }
    
    fileprivate func _presentModal(){
        _seenPromoScreen = true
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "PromoCodeVC") as! PromoCodeVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
    
    fileprivate func _tarifSelection(doUserHaveTarif: Bool){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            Session.sharedInstance.userHaveProfile = doUserHaveTarif
            ConsuptionRequests().doUserHaveTarif(gotTarif: doUserHaveTarif) { (done) in
                SwiftLoader.hide()
                if done {
                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "CurrentTarifInfoVC") as! CurrentTarifInfoVC
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(openNewVC, animated: true)
                    });
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
}

extension TarifSelectionVC {
    
    @IBAction func haveTarifBtn(_ sender: Any) {
        self._tarifSelection(doUserHaveTarif: true)
    }
    
    @IBAction func noTarifBtn(_ sender: Any) {
        self._tarifSelection(doUserHaveTarif: false)
    }
}
