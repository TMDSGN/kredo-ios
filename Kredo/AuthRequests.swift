//
//  AuthRequests.swift
//  Kredo
//
//  Created by Josef Antoni on 10.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthRequests {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getToken(completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/jwt-auth/v1/token"
        
        let para = [
            "username": "ios_token",
            "password": "YJ1doyTueAkGDk(I8n2Cw6kf"
        ]
        
        Alamofire.request(url, method: .post, parameters:para, encoding: JSONEncoding.default).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let token = json["token"].stringValue
                    if !token.isEmpty {
                        Session.sharedInstance.securityToken = token
                        completionHandler(true)
                    } else {
                        completionHandler(false)
                    }
                } else {
                    completionHandler(false)
                }
            } else {
                completionHandler(false)
            }
        }
    }
}
