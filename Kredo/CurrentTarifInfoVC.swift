//
//  CurrentTarifInfoVC.swift
//  Kredo
//
//  Created by Josef Antoni on 10.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import SwiftLoader
import NWSTokenView
import CoreTelephony

class CurrentTarifInfoVC: UIViewController, AlertMessage, UINavigationControllerDelegate {
    
    fileprivate let _providerDropDown = DropDown()
    fileprivate let _personTypeDropDown = DropDown()
    fileprivate var _individual = true
    let tokenViewMinHeight: CGFloat = 25.0
    let tokenViewMaxHeight: CGFloat = 150.0
    let tokenBackgroundColor = UIColor(red: 91.0/255.0, green: 151.0/255.0, blue: 244.0/255.0, alpha: 1.0)
    
    var contacts: [NWSTokenContact]!
    var selectedContacts = [NWSTokenContact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
        _setupDropDown()
        _registerObservers()
        _tokeninit()
        minutesTxtField.addTarget(self, action: #selector(textFieldDidBegin(_:)), for: .editingDidBegin)
        minutesTxtField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        smsTxtField.addTarget(self, action: #selector(textFieldDidBegin(_:)), for: .editingDidBegin)
        smsTxtField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        dataTxtField.addTarget(self, action: #selector(textFieldDidBegin(_:)), for: .editingDidBegin)
        dataTxtField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        monthyFeeTxtField.addTarget(self, action: #selector(textFieldDidBegin(_:)), for: .editingDidBegin)
        monthyFeeTxtField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
      self.view.endEditing(true)
    }
    
    func textFieldDidBegin(_ textField: UITextField) {
        if textField == minutesTxtField {
            minutesTxtField.text = ""
        }
        if textField == smsTxtField {
            smsTxtField.text = ""
        }
        if textField == dataTxtField {
            dataTxtField.text = ""
        }
        if textField == monthyFeeTxtField {
            monthyFeeTxtField.text = ""
        }
    }
    
    func textFieldDidEnd(_ textField: UITextField) {
        if textField == minutesTxtField && !minutesTxtField.text!.isEmpty {
            minutesTxtField.text = minutesTxtField.text! + " min"
        }
        if textField == smsTxtField && !smsTxtField.text!.isEmpty {
            smsTxtField.text = smsTxtField.text! + " sms"
        }
        if textField == dataTxtField && !dataTxtField.text!.isEmpty {
            dataTxtField.text = dataTxtField.text! + " GB"
        }
        if textField == monthyFeeTxtField && !monthyFeeTxtField.text!.isEmpty {
            monthyFeeTxtField.text = monthyFeeTxtField.text! + " Kč"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        self.tokenView.endEditing(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    fileprivate func _compare(){
        var tempLocation:[String] = []
        for i in 0..<selectedContacts.count {
            tempLocation.append(selectedContacts[i].name)
        }
        
        var provider = 1
        //set provider id to 1
        if let unpackageCarrierData = Session.sharedInstance.carrierData {
            if let newProvider: Int = unpackageCarrierData.nameArr.index(of: mobileProviderTxtField.text!) {
                //safely unwrap all potencionally nil variables and set new provider found in array
                provider = unpackageCarrierData.idArr[newProvider]
            }
        }
        //minute start
        if !minutesTxtField.text!.isEmpty {
            if let minutes = Double(minutesTxtField.text!.replacingOccurrences(of: " min", with: "")) {
                //sms start
                if !smsTxtField.text!.isEmpty {
                    if let sms = Double(smsTxtField.text!.replacingOccurrences(of: " sms", with: "")) {
                        //data start
                        if !dataTxtField.text!.isEmpty {
                            var dataTemp = (dataTxtField.text!.replacingOccurrences(of: " GB", with: ""))
                            if let data = Double(dataTemp.replacingOccurrences(of: ",", with: ".")) {
                                if !mobileProviderTxtField.text!.isEmpty {
                                                                            if !monthyFeeTxtField.text!.isEmpty {
                                            if !monthyFeeTxtField.text!.isEmpty {
                                                if !personTypeTxtField.text!.isEmpty {
                                                    //monthly fee start
                                                    if !monthyFeeTxtField.text!.isEmpty {
                                                        if let monthyFee = Double(monthyFeeTxtField.text!.replacingOccurrences(of: " Kč", with: "")) {
                                                            SwiftLoader.show(animated: true)
                                                            ConsuptionRequests().putUserInfo(minutes: minutes, sms: sms, data: data, carrier: provider, commonLocations: tempLocation, monthlySpend: monthyFee, individual: _individual, born: dateOfBirthTxtField.text!, ico: icoTxtField.text!, address: addressTxtField.text!, contact: contactPersonTxtField.text!) { (done) in
                                                                SwiftLoader.hide()
                                                                if done {
                                                                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "NumberOfDialVC") as! NumberOfDialVC
                                                                    DispatchQueue.main.async(execute: { () -> Void in
                                                                        self.navigationController?.pushViewController(openNewVC, animated: true)
                                                                    });
                                                                } else {
                                                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                                                                }
                                                            }
                                                        } else {
                                                            self.showSimpleAlert(title: "Chyba", message: "Měsíční výdaje musí obsahovat pouze číslice")
                                                        }
                                                    } else {
                                                        monthyFeeContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                                    }
                                                    //monthly fee end
                                                } else {
                                                    personTypeContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                                }
                                            } else {
                                                monthyFeeContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                            }
                                        } else {
                                            monthyFeeContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                        }
                                } else {
                                    mobileProviderContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                                }
                            } else {
                                self.showSimpleAlert(title: "Chyba", message: "Velikost dat musí obsahovat pouze číslice")
                            }
                        } else {
                            dataContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                        }
                        //data end
                    } else {
                        self.showSimpleAlert(title: "Chyba", message: "Počet sms musí obsahovat pouze číslice")
                    }
                } else {
                    smsContainer.shake(count: 4, for: 0.3, withTranslation: 8)
                }
                //sms end
            } else {
                self.showSimpleAlert(title: "Chyba", message: "Počet minut musí obsahovat pouze číslice")
            }
        } else {
            minutesContainer.shake(count: 4, for: 0.3, withTranslation: 8)
            _errorAnimeButton()
        }
        //minute end
    }
    
    fileprivate func _errorAnimeButton(){
        compareBtn.shake(count: 4, for: 0.3, withTranslation: 8)
        UIView.animate(withDuration: 1, animations: {
            self.compareBtn.backgroundColor = UIColor(red: 220.0/255, green: 46.0/255, blue: 97.0/255, alpha: 1.0)
        }) { (_) in
            UIView.animate(withDuration: 1, animations: {
            }) { (_) in
                UIView.animate(withDuration: 1, animations: {
                    self.compareBtn.backgroundColor = UIColor(red: 91.0/255, green: 151.0/255, blue: 244.0/255, alpha: 1.0)
                })
            }
        }
    }
    
    fileprivate func _setupDropDown(){
        self._providerDropDown.anchorView = self.mobileProviderContainer
        self._providerDropDown.direction = .bottom
        self._providerDropDown.bottomOffset = CGPoint(x: 0, y: self.mobileProviderContainer.bounds.height)
        if Session.sharedInstance.carrierData == nil {
            ConsuptionRequests().getCarriers(completionHandler: { (done, data) in
                if done {
                    Session.sharedInstance.carrierData = data
                    self._providerDropDown.dataSource = Session.sharedInstance.carrierData!.nameArr
                    self._setupCariertxtField()
                }
            })
        } else {
            self._providerDropDown.dataSource = Session.sharedInstance.carrierData!.nameArr
            self._setupCariertxtField()
        }
        self._providerDropDown.textFont = UIFont.systemFont(ofSize: 11)
        self._providerDropDown.backgroundColor = .white
        self._providerDropDown.cornerRadius = 23
        //personType
        self._personTypeDropDown.anchorView = self.personTypeContainer
        self._personTypeDropDown.direction = .bottom
        self._personTypeDropDown.bottomOffset = CGPoint(x: 0, y: self.personTypeContainer.bounds.height)
        self._personTypeDropDown.dataSource = ["Fyzická osoba", "Právnická osoba"]
        self._personTypeDropDown.textFont = UIFont.systemFont(ofSize: 11)
        self._personTypeDropDown.backgroundColor = .white
        self._personTypeDropDown.cornerRadius = 23
    }
    
    fileprivate func _setupCariertxtField(){
        if let carrierData = Session.sharedInstance.carrierData {
            self._providerDropDown.dataSource = carrierData.nameArr
            var carrier = CTTelephonyNetworkInfo().subscriberCellularProvider?.carrierName
            //var carrier = "Vodafone CZ"
            if carrier != nil {
                carrier = carrier!.replacingOccurrences(of: " - CZ", with: "").replacingOccurrences(of: " CZ", with: "")
                //Vodafone má zkratku "VF CZ"
                if (carrier=="VF"){
                    carrier = "Vodafone"
                }
                for i in 0..<carrierData.nameArr.count {
                    if carrierData.nameArr[i] == carrier {
                        self.mobileProviderTxtField.text = carrier
                        //self.dropDownIcon.isHidden = true
                        //self.dropDownBtn.isEnabled = false
                        //self.mobileProviderTxtField.isEnabled = false
                        self.dropDownIcon.isHidden = false
                        self.dropDownBtn.isEnabled = true
                        self.mobileProviderTxtField.isEnabled = true
                    }
                }
            }
        }
    }
    
    fileprivate func _setupScene(){
        //round corners
        self.minutesContainer.layer.cornerRadius = 20
        self.smsContainer.layer.cornerRadius = 20
        self.dataContainer.layer.cornerRadius = 20
        self.mobileProviderContainer.layer.cornerRadius = 20
        self.mostTargetingLocationContainer.layer.cornerRadius = 20
        self.monthyFeeContainer.layer.cornerRadius = 20
        self.personTypeContainer.layer.cornerRadius = 20
        self.dateOfBirthContainer.layer.cornerRadius = 20
        self.compareBtn.layer.cornerRadius = 20
        self.icoContainer.layer.cornerRadius = 20
        self.addressView.layer.cornerRadius = 20
        self.contactPersonView.layer.cornerRadius = 20
        //add shadows
        self.minutesContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.smsContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.dataContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.mobileProviderContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.mostTargetingLocationContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.monthyFeeContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.personTypeContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.dateOfBirthContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.compareBtn.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.icoContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.addressView.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        self.contactPersonView.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
        
        self.legalEntityView.alpha = 0
        self.legalEntityConstraints.isActive = false
    }
    
    fileprivate func _getProfileInfo() {
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileRequests().getProfileInfo { (done, myProfileData) in
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                if done {
                    openNewVC.myProfileData = myProfileData
                } else {
                    openNewVC.profileComplete = false
                }
                SwiftLoader.hide()
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @objc fileprivate func postDate(_ notification: NSNotification) {
        let dict = notification.object as! NSDictionary
        let selectedDate = dict["selectedDate"] as! String
        self.dateOfBirthTxtField.text = selectedDate
    }
    
    @objc fileprivate func pickedCity(_ notification: NSNotification) {
        let dict = notification.object as! NSDictionary
        let cityName = dict["cityName"] as! String
        let contact = NWSTokenContact(name: cityName)
        self.selectedContacts.append(contact)
        self.tokenView.reloadData()
    }
    
    fileprivate func _registerObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.postDate), name: NSNotification.Name(rawValue: "postDate"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pickedCity), name: NSNotification.Name(rawValue: "pickedCity"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "postDate"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "pickedCity"), object: nil)
    }
    
    func _tokeninit() {
        // TokenView
        tokenView.layoutIfNeeded()
        self.tokenView.layer.cornerRadius = 20
        tokenView.dataSource = self
        tokenView.delegate = self
        tokenView.reloadData()
    }
    
    @IBAction func didTapView(_ sender: Any) {
        tokenView.resignFirstResponder()
        tokenView.endEditing(true)
        self.view.endEditing(true)
    }
    
    // MARK: DZNEmptyDataSetSource
    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
        if let view = UINib(nibName: "EmptyDataSet", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView {
            view.frame = scrollView.bounds
            view.translatesAutoresizingMaskIntoConstraints = false
            view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
            return view
        }
        return nil
    }
    
    fileprivate func _openGooglePlaces(cityName: String){
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "CitiesTableVC") as! CitiesTableVC
        modal.firstInputChar = cityName
        if !mobileProviderTxtField.text!.isEmpty {
            modal.operatorName = mobileProviderTxtField.text!
        }
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
    
    //text field
    @IBOutlet weak var minutesTxtField: UITextField!
    @IBOutlet weak var smsTxtField: UITextField!
    @IBOutlet weak var dataTxtField: UITextField!
    @IBOutlet weak var mobileProviderTxtField: UITextField!
    @IBOutlet weak var monthyFeeTxtField: UITextField!
    @IBOutlet weak var personTypeTxtField: UITextField!
    @IBOutlet weak var dateOfBirthTxtField: UITextField!
    @IBOutlet var icoTxtField: UITextField!
    @IBOutlet var addressTxtField: UITextField!
    @IBOutlet var contactPersonTxtField: UITextField!
    
    //uiview
    @IBOutlet weak var minutesContainer: UIView!
    @IBOutlet weak var smsContainer: UIView!
    @IBOutlet weak var dataContainer: UIView!
    @IBOutlet weak var mobileProviderContainer: UIView!
    @IBOutlet weak var mostTargetingLocationContainer: UIView!
    @IBOutlet weak var monthyFeeContainer: UIView!
    @IBOutlet weak var personTypeContainer: UIView!
    @IBOutlet weak var dateOfBirthContainer: UIView!
    @IBOutlet var legalEntityView: UIView!
    @IBOutlet var icoContainer: UIView!
    @IBOutlet var addressView: UIView!
    @IBOutlet var contactPersonView: UIView!
    
    @IBOutlet weak var compareBtn: UIButton!
    @IBOutlet var dateOfBirthConstraint: NSLayoutConstraint!
    @IBOutlet var legalEntityConstraints: NSLayoutConstraint!
    @IBOutlet weak var tokenView: NWSTokenView!
    @IBOutlet var tokenViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var dropDownIcon: UIImageView!
    @IBOutlet var dropDownBtn: UIButton!
    
    @IBOutlet var mobileProviderinfoBtn: UIButton!
}

extension CurrentTarifInfoVC: NWSTokenDataSource, NWSTokenDelegate {
    
    // MARK: NWSTokenDataSource
    func numberOfTokensForTokenView(_ tokenView: NWSTokenView) -> Int {
        return selectedContacts.count
    }
    
    func insetsForTokenView(_ tokenView: NWSTokenView) -> UIEdgeInsets? {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
    func titleForTokenViewLabel(_ tokenView: NWSTokenView) -> String? {
        return nil
    }
    
    func titleForTokenViewPlaceholder(_ tokenView: NWSTokenView) -> String? {
        return "Nejčastější lokace:"
    }
    
    func tokenView(_ tokenView: NWSTokenView, viewForTokenAtIndex index: Int) -> UIView? {
        let contact = selectedContacts[Int(index)]
        if let token = NWSImageToken.initWithTitle(contact.name) {
            return token
        }
        return nil
    }
    
    // MARK: NWSTokenDelegate
    func tokenView(_ tokenView: NWSTokenView, didSelectTokenAtIndex index: Int) {
        let token = tokenView.tokenForIndex(index) as! NWSImageToken
        token.backgroundColor = UIColor(red: 133.0/255.0, green: 228.0/255.0, blue: 177.0/255.0, alpha: 1.0)
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeselectTokenAtIndex index: Int) {
        let token = tokenView.tokenForIndex(index) as! NWSImageToken
        token.backgroundColor = tokenBackgroundColor
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeleteTokenAtIndex index: Int) {
        // Ensure index is within bounds
        if index < self.selectedContacts.count {
            let contact = self.selectedContacts[Int(index)] as NWSTokenContact
            contact.isSelected = false
            self.selectedContacts.remove(at: Int(index))
            
            tokenView.reloadData()
            tokenView.layoutIfNeeded()
            tokenView.textView.becomeFirstResponder()
            
            // Check if search text exists, if so, reload table (i.e. user deleted a selected token by pressing an alphanumeric key)
            if tokenView.textView.text != ""
            {
                // self.searchContacts(tokenView.textView.text)
            }
        }
    }
    
    func tokenView(_ tokenView: NWSTokenView, didChangeText text: String) {
        // Check if empty (deleting text)
        _openGooglePlaces(cityName: text)
        tokenView.textView.text = ""
    }
    
    func tokenView(_ tokenView: NWSTokenView, didEnterText text: String) {
        if text == "" {
            return
        }
    }
    
    func tokenView(_ tokenView: NWSTokenView, contentSizeChanged size: CGSize) {
        self.tokenViewHeightConstraint.constant = max(tokenViewMinHeight,min(size.height, self.tokenViewMaxHeight))
        self.view.layoutIfNeeded()
        self.mostTargetingLocationContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
    }
    
    func tokenView(_ tokenViewDidBeginEditing: NWSTokenView) {
    }
    
    func tokenViewDidEndEditing(_ tokenView: NWSTokenView) {
    }
    
    func tokenView(_ tokenView: NWSTokenView, didFinishLoadingTokens tokenCount: Int) {
    }
}

class NWSTokenContact: NSObject {
    var name: String!
    var isSelected = false
    
    init(name: String)
    {
        self.name = name
    }
    
    class func sortedContacts(_ contacts: [NWSTokenContact]) -> [NWSTokenContact]
    {
        return contacts.sorted(by: { (first, second) -> Bool in
            return first.name < second.name
        })
    }
}

extension CurrentTarifInfoVC {
    
    @IBAction func mobileProviderInfoBtn(_ sender: Any) {
        if !self.dropDownIcon.isHidden {
            self.showSimpleAlert(title: "Určení operátora", message: "Využíváš síť tohoto mobilního operátora.")
        }
    }

    @IBAction func locationInfoBtn(_ sender: Any) {
        self.showSimpleAlert(title: "Běžná lokace", message: "Pro určení síly signálu ve tvé lokalitě.")
    }
    
    @IBAction func openProfileBtn(_ sender: Any) {
        _getProfileInfo()
    }
    
    @IBAction func mobileProviderBtn(_ sender: Any) {
        self.view.endEditing(false)
        if _providerDropDown.isHidden {
            self._providerDropDown.show()
        }
        self._providerDropDown.selectionAction = { [unowned self] (index, item) in
            self.mobileProviderTxtField.text = item
        }
    }
    
    @IBAction func personTypeBtn(_ sender: Any) {
        self.view.endEditing(false)
        if _personTypeDropDown.isHidden {
            _personTypeDropDown.show()
        }
        self._personTypeDropDown.selectionAction = { [unowned self] (index, item) in
            self.personTypeTxtField.text = item
            if self.personTypeTxtField.text == "Fyzická osoba" {
                self._individual = true
                UIView.animate(withDuration: 0.5, animations: {
                    self.legalEntityView.alpha = 0
                    self.dateOfBirthConstraint.isActive = true
                    self.legalEntityConstraints.isActive = false
                    self.view.layoutIfNeeded()
                })
            } else {
                self._individual = false
                UIView.animate(withDuration: 0.5, animations: {
                    self.legalEntityView.alpha = 1
                    self.dateOfBirthConstraint.isActive = false
                    self.legalEntityConstraints.isActive = true
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @IBAction func selectBirthDateBtn(_ sender: Any) {
        self.view.endEditing(false)
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        modal.birthDatePicker = true
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
    
    @IBAction func compareBtn(_ sender: Any) {
        self.view.endEditing(false)
        _compare()
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

public extension UISearchBar {
    public func setTextColor(_ color: UIColor) {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.textColor = color
    }
}

