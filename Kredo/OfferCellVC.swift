//
//  OfferCellVC.swift
//  Kredo
//
//  Created by Josef Antoni on 12.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class OfferCellVC: UITableViewCell {
    
    var id: String = ""
    var cantBeOrdered = false
    
    override func awakeFromNib() {
        self.greenBtn.layer.cornerRadius = 20
        self.blueBtn.layer.cornerRadius = 20
        self.mainContainer.layer.cornerRadius = 20
        
        //shadow po celém kontajneru
        self.mainContainer.layer.shadowOpacity = 0.7
        self.mainContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.mainContainer.layer.shadowRadius = 8
        self.mainContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
    }

    @IBOutlet var providerLogo: UIImageView!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var greenBtn: UIButton!
    @IBOutlet weak var blueBtn: UIButton!
    
    @IBOutlet var minuteLabel: UILabel!
    @IBOutlet var smsLabel: UILabel!
    @IBOutlet var dataLabel: UILabel!
    @IBOutlet var coverLabel: UILabel!
    
    @IBOutlet var myMinutesLabel: UILabel!
    @IBOutlet var myTextLabel: UILabel!
    @IBOutlet var myDataLabel: UILabel!
    @IBOutlet var myCoverLabel: UILabel!
    
    @IBOutlet var totalPriceLabel: UILabel!
    @IBOutlet var cheaperRedLabel: UILabel!
    
    @IBOutlet var wifiTarificon: UIImageView!
    @IBOutlet var pcTarifIcon: UIImageView!
}

extension OfferCellVC {
    
    @IBAction func openDetailBtn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openTarifDetail"), object: ["objectId" : id])
    }
    
    @IBAction func iWantThatBtn(_ sender: Any) {
        if cantBeOrdered {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cantBeOrdered"), object: nil)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderTarifDetail"), object: ["objectId" : id])
        }
    }
}
