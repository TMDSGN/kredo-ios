//
//  PasswordResetVC.swift
//  Kredo
//
//  Created by Josef Antoni on 15.08.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class PasswordResetVC: UIViewController, AlertMessage {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
    }
    
    fileprivate func _setupScene(){
        passBtn.layer.cornerRadius = passBtn.frame.size.height/2
        passBtn.layer.borderWidth = 1
        passBtn.layer.borderColor = UIColor.white.cgColor
        emailContainer.layer.cornerRadius = emailContainer.frame.size.height/2
        emailContainer.addShadow(offset: CGSize.init(width: 0, height: 0), color: UIColor.black, radius: 10.0, opacity: 0.10)
    }
    
    @IBOutlet var emailContainer: UIView!
    @IBOutlet var passBtn: UIButton!
    @IBOutlet var emailtxtField: UITextField!
}

extension PasswordResetVC {
    
    @IBAction func resetBtn(_ sender: Any) {
        if !emailtxtField.text!.isEmpty {
            if emailtxtField.text!.isValidEmail() {
                SwiftLoader.show(animated: true)
                LoginRequests().resetPass(email: emailtxtField.text!, completionHandler: { (done) in
                    SwiftLoader.hide()
                    if done {
                        let alert = UIAlertController(title: "Hotovo", message: "Zkontrolujte email pro nové heslo.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                            self.dismiss(animated: true , completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.showSimpleAlert(title: "Email nenalezen", message: "Překontrolujte vložený email.")
                    }
                })
            } else {
                self.showSimpleAlert(title: "Chyba", message: "Zadejte Email ve správném formátu")
            }
        } else {
            emailContainer.shake(count: 4, for: 0.3, withTranslation: 8)
        }
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
