//
//  Loader.swift
//  Kredo
//
//  Created by Josef Antoni on 11.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import SwiftLoader
import UIKit

class Loader {
    
    /*
     *   Setup loader
     */
    func configLoader(){
        var config : SwiftLoader.Config = SwiftLoader.Config()
        config.size = 100
        config.spinnerColor = UIColor(red: 91.0/255, green: 151.0/255, blue: 244.0/255, alpha: 1.0)
        config.spinnerLineWidth = 1.5
        //pozadí
        config.backgroundColor = UIColor.clear
        config.foregroundColor = UIColor.black
        config.foregroundAlpha = 0.0
        config.cornerRadius = 50
        SwiftLoader.setConfig(config)
    }
}
