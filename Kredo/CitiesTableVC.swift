//
//  CitiesTableVC.swift
//  Kredo
//
//  Created by Josef Antoni on 21.09.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class CitiesTableVC: UIViewController, UISearchBarDelegate {
    
    var operatorName = "Vodafone"
    var firstInputChar = ""
    fileprivate let cityData: CityData = CityData()
    
    override func viewDidLoad() {
        _customCellRegister()
        cityData.getItems("Operator = '\(operatorName)' && Mesto BEGINSWITH '\(firstInputChar.capitalized)'")
        pickCity.layer.cornerRadius = pickCity.frame.height/2
        searchBar.barTintColor = .clear
        searchBar.backgroundImage = UIImage()
        _registerObservers()
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.bottomTableConstraint.constant = keyboardSize.height
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.bottomTableConstraint.constant = 80
    }
    override func viewWillAppear(_ animated: Bool) {
        self.searchBar.text = firstInputChar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    fileprivate func _customCellRegister(){
        let nib = UINib(nibName: "ProfileTableCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ProfileTableCell")
        tableView.separatorStyle = .none
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBar.text = searchBar.text!.firstUppercased

        cityData.getItems("Operator = '\(operatorName)' && Mesto BEGINSWITH '\(searchBar.text!)'")
        tableView.reloadData()
    }
    
    fileprivate func _registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var pickCity: UIButton!
    @IBOutlet var bottomTableConstraint: NSLayoutConstraint!
}

extension CitiesTableVC {
    
    @IBAction func cancelBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func pickCity(_ sender: Any) {
        if !searchBar.text!.isEmpty {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pickedCity"), object: ["cityName" : self.searchBar.text!])
            dismiss(animated: true, completion: nil)
        }
    }

}

extension CitiesTableVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityData.datasource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell     {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProfileTableCell") as! ProfileTableCellVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.promoName.text = cityData.datasource[indexPath.row].Mesto
        cell.promoName.textColor = .white
        cell.promoIcon.image = UIImage()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchBar.text! = cityData.datasource[indexPath.row].Mesto
        self.view.endEditing(true)
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension String {
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
}
