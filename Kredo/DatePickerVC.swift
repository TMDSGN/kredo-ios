//
//  DatePickerVC.swift
//  Kredo
//
//  Created by Josef Antoni on 19.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class DatePickerVC: UIViewController {
    
    fileprivate var _selectedDate: String = ""
    var personalInfoDate = false
    var kontraktEndView = false
    var birthDatePicker = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
        _currentDate()
        
        if personalInfoDate {
            self.datePicker.minimumDate = Date(timeIntervalSinceNow: (((24 * 60) * 60) * 1))
        }
        if birthDatePicker {
            self.datePicker.maximumDate = Date()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.dateContainer.alpha = 1
            self.view.alpha = 1
        }
    }
    
    @objc fileprivate func dateChanged(_ sender: UIDatePicker) {
        _currentDate()
    }
    
    @IBAction func dateChangeAction(_ sender: Any) {
        if personalInfoDate {
            let curentDate = datePicker.date
            if curentDate.isWeekend {
                let datePlus = Calendar.current.date(byAdding: .day, value: 1, to: curentDate)!
                if datePlus.isWeekend {
                    datePicker.date = Calendar.current.date(byAdding: .day, value: 2, to: curentDate)!
                } else {
                    datePicker.date = datePlus
                }
            }
        }
    }
    
    fileprivate func _currentDate(){
        if !personalInfoDate {
            let componenets = Calendar.current.dateComponents([.year, .month, .day], from: datePicker.date)
            if let day = componenets.day, let month = componenets.month, let year = componenets.year {
                _selectedDate = "\(day).\(month).\(year)"
            }
        } else {
            datePicker.datePickerMode = UIDatePickerMode.dateAndTime
            let componenets = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: datePicker.date)
            if let day = componenets.day, let month = componenets.month, let year = componenets.year, let hour = componenets.hour, let minute = componenets.minute {
                _selectedDate = "\(hour):\(minute) \(day).\(month).\(year)"
            }
        }
    }
    
    fileprivate func _setupScene(){
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        self.backBtn.layer.cornerRadius = 20
        self.dateContainer.layer.cornerRadius = 20
        self.dateContainer.alpha = 0
        self.view.alpha = 0
    }
    
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var dateContainer: UIView!
}

extension DatePickerVC {
    
    @IBAction func backBtn(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.dateContainer.alpha = 0
            self.view.alpha = 0
        }) { (_) in
            self.dismiss(animated: false) {
                if self.personalInfoDate {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postDatePersonalInfo"), object: ["selectedDate" : self._selectedDate, "kontraktEndSelected" : self.kontraktEndView])
                } else if self.kontraktEndView {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postDatePersonalInfo"), object: ["selectedDate" : self._selectedDate, "kontraktEndSelected" : self.kontraktEndView])
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postDate"), object: ["selectedDate" : self._selectedDate])
                }
            }
        }
    }
}

extension Date {
    var isWeekend: Bool {
        return NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!.isDateInWeekend(self)
    }
}
