//
//  ContractCreatedVC.swift
//  Kredo
//
//  Created by Josef Antoni on 18.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class ContractCreatedVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    fileprivate func _setupScene(){
        self.goToProfile.layer.cornerRadius = 20
    }
    
    fileprivate func _getProfileInfo(){
        if Reachability.isConnectedToNetwork() {
            ProfileRequests().getProfileInfo { (done, myProfileData) in
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                if done {
                    openNewVC.myProfileData = myProfileData
                } else {
                    openNewVC.profileComplete = false
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @IBOutlet var goToProfile: UIView!
}

extension ContractCreatedVC {
    
    @IBAction func goToProfileNavBtn(_ sender: Any) {
        _getProfileInfo()
    }
    
    @IBAction func goToProfile(_ sender: Any) {
        _getProfileInfo()
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}
