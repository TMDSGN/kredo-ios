//
//  MyProfileVC.swift
//  Kredo
//
//  Created by Josef Antoni on 22.05.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import MessageUI
import SwiftLoader


class MyProfileVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, MFMailComposeViewControllerDelegate, AlertMessage {
    
    var profileComplete = true
    var tableShowed = false
    var creditTextShowed = false
    var myProfileData: MyProfileData?
    private var imagePicker = UIImagePickerController()
    @IBOutlet weak var deleteButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = myProfileData {
            profileComplete = data.finishedAccount
        }
        _customCellRegister()
        _setupScene()
        _profileInit()
        _getProfileInfo()
        
        deleteButton.setTitle("Smazat účet",for: .normal)
        deleteButton.setTitleColor(UIColor.red, for: .normal)
        
    }
    
    fileprivate func _showTable(show: Bool){
        if myProfileData != nil {
            if myProfileData!.promoNameArr.count > 0 {
                var profileDataCount: Int = myProfileData!.promoNameArr.count*40
                var tableViewAlpha: CGFloat = 0
                var creditTxtView: CGFloat = 0
                if show && !tableShowed {
                    tableShowed = true
                    creditTextShowed = false
                    tableViewAlpha = 1
                    creditTxtView = 0
                } else {
                    profileDataCount = myProfileData!.promoNameArr.count/40
                    tableShowed = false
                    tableViewAlpha = 0
                    creditTxtView = 0
                }
                tableView.reloadData()
                self.completeProfileBtnConstrait.constant = CGFloat(260 + profileDataCount)
                self.tableViewHeight.constant = CGFloat(profileDataCount)
                self.addShadowToTable()
                UIView.animate(withDuration: 0.5, animations: {
                    self.tableView.alpha = tableViewAlpha
                    self.creditsTxtView.alpha = creditTxtView
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    fileprivate func _showCreditText(show: Bool){
        self.creditsTxtView.alpha = 0
        creditsTxtView.sizeToFit()
        var profileDataCount: CGFloat = self.creditsTxtView.frame.height
        var tableViewAlpha: CGFloat = 0
        var creditTxtView: CGFloat = 0
        if show && !creditTextShowed {
            creditTextShowed = true
            tableShowed = false
            tableViewAlpha = 0
            creditTxtView = 1
            self.completeProfileBtnConstrait.constant = 260 + profileDataCount + 30
            self.kreditsSendContainer.isHidden = false
        } else {
            profileDataCount = 0
            creditTextShowed = false
            tableViewAlpha = 0
            creditTxtView = 0
            self.completeProfileBtnConstrait.constant = 260 + profileDataCount
            self.kreditsSendContainer.isHidden = true
        }
        self.tableViewHeight.constant = profileDataCount
        self.addShadowToTable()
        UIView.animate(withDuration: 0.5, animations: {
            self.tableView.alpha = tableViewAlpha
            self.creditsTxtView.alpha = creditTxtView
            self.view.layoutIfNeeded()
        })
    }
    
    fileprivate func addShadowToTable(){
        self.tableViewContainer.layer.cornerRadius = 20
        self.tableViewContainer.layer.shadowOpacity = 0.7
        self.tableViewContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.tableViewContainer.layer.shadowRadius = 8
        self.tableViewContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
    }
    
    fileprivate func _getProfileInfo(){
        if let url = NSURL(string: myProfileData!.photo) {
            if let data = NSData(contentsOf: url as URL) {
                self.profilePhoto.image = UIImage(data: data as Data)
            }
        }
        if profileComplete {
            self.nameLabel.text! = "\(myProfileData!.firstName) \(myProfileData!.lastName)"
            if myProfileData!.phoneNumber == "null" {
                self.phoneLabel.text! = "telefonní číslo"
            } else {
                self.phoneLabel.text! = myProfileData!.phoneNumber
            }
            if myProfileData!.newMin == "neomezeně" {
                self.myMinutesLabel.text = "\u{221E} min"
            } else {
                self.myMinutesLabel.text = myProfileData!.newMin + " min"
            }
            if myProfileData!.newSms == "neomezeně" {
                self.mySmsLabel.text = "\u{221E}"
            } else {
                self.mySmsLabel.text = myProfileData!.newSms
            }
            self.myDataLabel.text! = myProfileData!.newData + " GB"
            
            if myProfileData!.oldMin == "neomezeně" {
                self.oldMinuteLabel.text = "\u{221E} min"
            } else {
                self.oldMinuteLabel.text = myProfileData!.oldMin + " min"
            }
            if myProfileData!.oldSms == "neomezeně" {
                self.oldSmsLabel.text = "\u{221E}"
            } else {
                self.oldSmsLabel.text = myProfileData!.oldSms
            }
            self.oldDataLabel.text! = myProfileData!.oldData + " GB"
            self.myPromoCodeLabel.text! = myProfileData!.promoCode
            self.promocodeUsedLabel.text! = myProfileData!.promoUsed
            self.myCreditsLabel.text! = myProfileData!.kredits
            self.redLabel.text! = "S námi si ušetřil již \(myProfileData!.saved) Kč"
            self.oldCoverLabel.text = myProfileData!.oldCost + " Kč"
            self.myCoverLabel.text = myProfileData!.newCost + " Kč"
            if myProfileData!.buttonShow {
                self.firstBlueBtn.isHidden = false
                self.makeContractOnceAgainConstraint.constant = 115
            } else {
                self.firstBlueBtn.isHidden = true
                self.makeContractOnceAgainConstraint.constant = 40
            }
        }
    }
    
    fileprivate func _profileInit(){
        self.kreditsSendContainer.isHidden = true
        if profileComplete {
            self.firstBlueBtn.isHidden = true
            self.myPromoCodeTitleLabel.isHidden = false
            self.uSavedWithUsConstant.constant = 115
            self.redLabel.text = "S námi si ušetřil již 0 Kč"
            self.ppeopleUserPromoCodeContainer.isHidden = false
            self.creditsContainer.isHidden = false
            self.completeProfileBtnConstrait.isActive = true
            self.completeProfileBtnConstrait.constant = 260
            self.navigationController?.navigationItem.leftBarButtonItem = nil
            self.twoContainersHeight.constant = self.ppeopleUserPromoCodeContainer.frame.width
            let item = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
            navigationItem.leftBarButtonItem = item
            self.view.layoutIfNeeded()
        } else {
            self.myPromoCodeContainer.isHidden = true
            self.myPromoCodeTitleLabel.isHidden = true
            self.firstBlueBtn.isHidden = true
            self.uSavedWithUsConstant.constant = 30
            self.redLabel.text = "Bohužel jestě si s námi\nnic neušetřil"
            self.ppeopleUserPromoCodeContainer.isHidden = true
            self.creditsContainer.isHidden = true
            //  self.completeProfileBtnConstrait.isActive = false
            self.completeProfileBtnConstrait.constant = 30
            self.myPromoCodeIncompleteConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func _setupScene(){
        self.navigationController?.isNavigationBarHidden = false
        self.profilePhoto.layer.cornerRadius = self.profilePhoto.frame.height/2
        self.profilePhoto.clipsToBounds = true
        //round corners
        self.firstBlueBtn.layer.cornerRadius = 23
        self.resourcesNowContainer.layer.cornerRadius = 20
        self.resourcesPastContainer.layer.cornerRadius = 20
        self.myPromoCodeContainer.layer.cornerRadius = 20
        self.ppeopleUserPromoCodeContainer.layer.cornerRadius = 20
        self.creditsContainer.layer.cornerRadius = 20
        self.invFriendBtn.layer.cornerRadius = 23
        self.shareFbBtn.layer.cornerRadius = 23
        self.kreditsSendContainer.layer.cornerRadius = 15
        //shadow po celém kontajneru
        self.kreditsSendContainer.layer.shadowOpacity = 0.7
        self.kreditsSendContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.kreditsSendContainer.layer.shadowRadius = 8
        self.kreditsSendContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor

        self.resourcesNowContainer.layer.shadowOpacity = 0.7
        self.resourcesNowContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.resourcesNowContainer.layer.shadowRadius = 8
        self.resourcesNowContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        self.resourcesPastContainer.layer.shadowOpacity = 0.7
        self.resourcesPastContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.resourcesPastContainer.layer.shadowRadius = 8
        self.resourcesPastContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        self.myPromoCodeContainer.layer.shadowOpacity = 0.7
        self.myPromoCodeContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.myPromoCodeContainer.layer.shadowRadius = 8
        self.myPromoCodeContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        self.ppeopleUserPromoCodeContainer.layer.shadowOpacity = 0.7
        self.ppeopleUserPromoCodeContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.ppeopleUserPromoCodeContainer.layer.shadowRadius = 8
        self.ppeopleUserPromoCodeContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
        self.creditsContainer.layer.shadowOpacity = 0.7
        self.creditsContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.creditsContainer.layer.shadowRadius = 8
        self.creditsContainer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.2).cgColor
    }
    
    fileprivate func _customCellRegister(){
        let nib = UINib(nibName: "ProfileTableCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ProfileTableCell")
        tableView.separatorStyle = .none
    }
    
    @objc fileprivate func dismissView(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func openLibrary(){
        DelayFunc.sharedInstance.delay(0.3) {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                self.imagePicker.navigationBar.barTintColor = .white
                self.imagePicker.navigationBar.isTranslucent = false
                self.imagePicker.navigationBar.tintColor = UIColor(red: 91.0/255.0, green: 151.0/255.0, blue: 244.0/255.0, alpha: 1.0)
                self.imagePicker.navigationBar.titleTextAttributes = [
                    NSForegroundColorAttributeName : UIColor(red: 91.0/255.0, green: 151.0/255.0, blue: 244.0/255.0, alpha: 1.0)
                ]
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: nil)
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.layer.zPosition = 1
        if let img = image {
            profilePhoto.image = img.resizeWithWidth(200)
            profilePhoto.layer.masksToBounds = false
            profilePhoto.layer.cornerRadius = profilePhoto.frame.size.width/2
            profilePhoto.clipsToBounds = true
            let str  = img.resizeWithWidth(200)!.toBase64()
            ProfileRequests().updateNewPhoto(photo: str, completionHandler: { (_) in })
        }
    }
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print("Did complete sharing.. ")
    }
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        print("Error tool place in appInviteDialog \(error)")
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setSubject("Pozvání do aplikace Kréďo")
        if let code = myProfileData {
            mailComposerVC.setMessageBody("Můj promo kód: \(code.promoCode)\nUšetři za mobil. Vyber si nejlepší tarif od mobilního operátora a plať méně.", isHTML: false)
        }
        return mailComposerVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var firstBlueBtn: UIButton!
    @IBOutlet var resourcesNowContainer: UIView!
    @IBOutlet var resourcesPastContainer: UIView!
    @IBOutlet var myPromoCodeContainer: UIView!
    @IBOutlet var ppeopleUserPromoCodeContainer: UIView!
    @IBOutlet var creditsContainer: UIView!
    @IBOutlet var invFriendBtn: UIButton!
    @IBOutlet var shareFbBtn: UIButton!
    @IBOutlet var twoContainersHeight: NSLayoutConstraint!
    @IBOutlet var profilePhoto: UIImageView!
    
    @IBOutlet var redLabel: UILabel!
    @IBOutlet var myPromoCodeLabel: UITextView!
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    
    @IBOutlet var myMinutesLabel: UILabel!
    @IBOutlet var mySmsLabel: UILabel!
    @IBOutlet var myDataLabel: UILabel!
    @IBOutlet var myCoverLabel: UILabel!
    
    @IBOutlet var oldMinuteLabel: UILabel!
    @IBOutlet var oldSmsLabel: UILabel!
    @IBOutlet var oldDataLabel: UILabel!
    @IBOutlet var oldCoverLabel: UILabel!
    @IBOutlet var promocodeUsedLabel: UILabel!
    @IBOutlet var myCreditsLabel: UILabel!
    @IBOutlet var myPromoCodeTitleLabel: UILabel!
    @IBOutlet var myPromoCodeIncompleteConstraint: NSLayoutConstraint!
    
    //constraints
    @IBOutlet var uSavedWithUsConstant: NSLayoutConstraint!
    @IBOutlet var completeProfileBtnConstrait: NSLayoutConstraint!
    @IBOutlet var makeContractOnceAgainConstraint: NSLayoutConstraint!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    @IBOutlet var tableViewContainer: UIView!
    @IBOutlet var creditsTxtView: UITextView!
    
    @IBOutlet var kreditsSendContainer: UIView!
    @IBOutlet var bankNumberTxtField: UITextField!
}

extension MyProfileVC: FBSDKAppInviteDialogDelegate {
    
    @IBAction func sendBankNumberBtn(_ sender: Any) {
        if !bankNumberTxtField.text!.isEmpty {
            ProfileRequests().sendBankAccountNumber(number: bankNumberTxtField.text!) { (done) in
                if done {
                    self.showSimpleAlert(title: "Hotovo!", message: "Vaše číslo účtu bylo úspěšně nahráno do systému.")
                }else{
                     self.showSimpleAlert(title: "Nedostatečný kredit.", message: "Pro vyplacení kreditu je třeba mít minimálně 1000 kreditů.")
                }
            }
        }
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    @IBAction func uploadPhotoBtn(_ sender: Any) {
        if !Session.sharedInstance.facebookUser {
            openLibrary()
        }
    }
    
    @IBAction func showTableBtn(_ sender: Any) {
        _showTable(show: true)
    }
    
    @IBAction func showCreditTextBtn(_ sender: Any) {
        _showCreditText(show: true)
    }
    
    @IBAction func shareFbBtn(_ sender: Any) {
        if let link = myProfileData {
            let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
            content.contentURL = NSURL(string: link.shareLink)! as URL
            let dialog : FBSDKShareDialog = FBSDKShareDialog()
            dialog.fromViewController = self
            dialog.shareContent = content
            dialog.mode = FBSDKShareDialogMode.feedWeb
            dialog.show()
        }
    }
    
    @IBAction func inviteFriendBtn(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        mailComposeViewController.navigationBar.tintColor = UIColor(red: 91.0/255.0, green: 151.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func signOutBtn(_ sender: Any) {
        view.endEditing(true)
        let alert = UIAlertController(title: "Odhlášení", message: "Opravdu se chcete odhlásit ze svého účtu?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ne", style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Ano", style: UIAlertActionStyle.default, handler: { action in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension MyProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let countRows = myProfileData {
            return countRows.promoNameArr.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProfileTableCell") as! ProfileTableCellVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.promoName.text = myProfileData?.promoNameArr[indexPath.row]
        if let url = NSURL(string: myProfileData!.promoAvatarArr[indexPath.row]) {
            if let data = NSData(contentsOf: url as URL) {
                cell.promoIcon.image = UIImage(data: data as Data)
                cell.promoIcon.layer.cornerRadius = cell.promoIcon.frame.height/2
                cell.promoIcon.clipsToBounds = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        let alert = UIAlertController(title: "Pozor!", message: "Opravdu chcete smazat celý svůj profil? Ztratíte tím nárok na případné výplaty bonusů. ", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ne", style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Ano", style: .destructive, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                SwiftLoader.show(animated: true)
                LoginRequests().deleteUser() { (done, errMessage) in
                    if done {
                        SwiftLoader.hide()
                        self.navigationController?.popToRootViewController(animated: true)
                    } else if errMessage != nil {
                        SwiftLoader.hide()
                        self.showSimpleAlert(title: "Chyba", message: errMessage!)
                    } else {
                        SwiftLoader.hide()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                    }
                }
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
}
